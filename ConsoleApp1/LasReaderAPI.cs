﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using LASReader = System.IntPtr;

namespace LasReader
{
    internal class CAPI
    {
        public const string LasReaderDLL = @"LasReaderDLL.dll";
        [DllImport(LasReaderDLL, CharSet = CharSet.Unicode, ThrowOnUnmappableChar = true)]
        public static extern LASReader createLasReader(string file, int cf);
        [DllImport(LasReaderDLL, CharSet = CharSet.Unicode)]
        public static extern void destroyLasReader(LASReader hId);
        [DllImport(LasReaderDLL, CharSet = CharSet.Ansi)]
        public static extern Int64 getNumberOfPoints(LASReader laSReader);
        [DllImport(LasReaderDLL, CharSet = CharSet.Ansi)]
        public static extern bool getNextPoint(LASReader laSReader);
        [DllImport(LasReaderDLL)]
        public static extern void getFields(LASReader laSReader, out double x, out double y, out double z, out double gpsTime, out byte classification);
        [DllImport(LasReaderDLL)]
        public static extern void getFields2(LASReader laSReader, out double x, out double y, out double z, out double gpsTime, out byte classification, out ulong pointSourceID, out byte intensity);
        [DllImport(LasReaderDLL)]
        public static extern void convertFields(LASReader laSReader, double x, double y, double z, out double X, out double Y, out double Z);
        [DllImport(LasReaderDLL)]
        public static extern void seek(LASReader laSReader, int position);
    }
}
