﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static DateTime GetFromGps(double weeknumber, double seconds)
        {
            DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
            DateTime dt = datum.AddDays(weeknumber * 7);
            //dt = dt.AddSeconds(seconds);
            return dt;
        }
        static DateTime GetFromGps(double gpsTime)
        {
            /*
            I32 week = (I32)(point->gps_time/604800.0 + 1653.4391534391534391534391534392);
            I32 secs = week*604800 - 1000000000;
            point->gps_time -= secs;
            -----------------
            inline void transform(LASpoint* point) const { point->gps_time += delta_secs; }
            LASoperationConvertWeekToAdjustedGps(I32 week) { this->week = week; delta_secs = week; delta_secs *= 604800; delta_secs -= 1000000000; };
             */
            //DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
            //gpsTime -= 9000000;
            //double week = gpsTime / 604800.0 + 1653.4391534391534391534391534392;
            double week = gpsTime / 604800.0 + 1653.4391534391534391534391534392;
            double secs = week * 604800 - 1000000000;
            //double secs = gpsTime % (TimeSpan.FromDays(1).TotalSeconds);

            return GetFromGps(week, secs);
        }
        static void Main(string[] args)
        {
            string lasPath = @"C:\LAS_DATA\1187-pp001-2018-06-29-08-24-34\scans_WGS84_All.las";
            var en = new LasEn(lasPath);
            long i = 0;
            double tt = 0;
            int cc = 0;
            while (en.MoveNext())
            {
                var c1 = en.Current;
                LasReader.CAPI.convertFields(en._lr, c1.X, c1.Y, c1.Z, out c1.X, out c1.Y, out c1.Z);
                cc++;
                if (tt != c1.GpsTime)
                {
                    tt = c1.GpsTime;
                    var dt = GetFromGps(tt);
                    cc = 0;
                    Console.Title = $"{dt}";
                }
                if (i == 1000000)
                    break;
                i++;
                if (i % 1000 == 0)
                    Console.Title = $"{i}, {((double)i).ToString("0.0%")}";
            }
            //LasReader.CAPI.CreateTilesData(lasPath, lasPath, lasPath);
            // var i = LasReader.CAPI.test();
            var lr = LasReader.CAPI.createLasReader(lasPath, 2);
            var count = LasReader.CAPI.getNumberOfPoints(lr);
            //var next = LasReader.CAPI.getNextPoint(lr);
            LasReader.CAPI.seek(lr, 30000000);
            double x = 0,y = 0,z = 0,t = 0;
            byte c = 0;
            LasReader.CAPI.getFields(lr, out x, out y, out z, out t, out c);
            //int week = (int)Math.Floor(t);
            //double seconds = t - Math.Floor(t);
            //DateTime dt = GetFromGps(week, seconds);
            
            DateTime t1 = DateTime.Now;
            //long i = 0;
            while (LasReader.CAPI.getNextPoint(lr))
            {
                if (i == 1000000)
                    break;
                i++;
                if (i % 1000 == 0)
                    Console.Title = $"{count}, {i}, {((double)i / count).ToString("0.0%")}";
            }
            LasReader.CAPI.getFields(lr, out x, out y, out z, out t, out c);

            var ts = DateTime.Now - t1;
            Console.WriteLine(ts);
            LasReader.CAPI.destroyLasReader(lr);
            //new System.IO.FileInfo(@"D:\1187-10009-2015-06-07-13-09-18\scans_WGS84_All.las")
            //var x = File.ReadLines(@"D:\1187-10009-2015-06-07-13-09-18\scans_WGS84_All.las").Take(2000).ToArray();
            //var linesCount = File.ReadLines(@"D:\1187-10009-2015-06-07-13-09-18\scans_WGS84_All.las")
            //.Count();
            Console.ReadKey();
                
        }
    }
}
