﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LASReader = System.IntPtr;
namespace ConsoleApp1
{
    public class LasPoint
    {
        public double X;
        public double Y;
        public double Z;
        public double GpsTime;
        public byte Classification;
        public ulong PointSourceID;
        public byte Intensity;
    }
    public class LasEn : IEnumerator<LasPoint>
    {
        public LASReader _lr = IntPtr.Zero;
        public LasEn(string path)
        {
            _lr = LasReader.CAPI.createLasReader(path, 2);
        }
        public LasPoint Current
        {
            get
            {
                var p = new LasPoint();
                LasReader.CAPI.getFields2(_lr, out p.X, out p.Y, out p.Z, out p.GpsTime, out p.Classification, out p.PointSourceID, out p.Intensity);
                return p;
            }
        }

        object IEnumerator.Current => this.Current;

        public bool MoveNext()
        {
            return LasReader.CAPI.getNextPoint(_lr);
        }

        public void Reset()
        {
            LasReader.CAPI.seek(_lr, 0);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LasEn() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            if (_lr != IntPtr.Zero)
            {
                LasReader.CAPI.destroyLasReader(_lr);
                _lr = IntPtr.Zero;
            }
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
