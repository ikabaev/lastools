﻿using Microsoft.Win32;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Core;
using SharpGL.SceneGraph.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFOpenGl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {
            //OpenGL gl = args.OpenGL;

            //gl.Enable(OpenGL.GL_DEPTH_TEST);

            //float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            //float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            //float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            //float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            //float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            //float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            //gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            //gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            //gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            //gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            //gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            //gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            //gl.Enable(OpenGL.GL_LIGHTING);
            //gl.Enable(OpenGL.GL_LIGHT0);

            //gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        float rotation = 0;
        private Axies axies = new Axies();
        private void OpenGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            
            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            axies.Render(gl, RenderMode.Design);
            //// Move Left And Into The Screen
            
            //gl.Translate(0.0f, 0.0f, -6.0f);


            //gl.Rotate(rotation, 0.0f, 1.0f, 0.0f);
            //var scale = .1;
            //gl.PushAttrib(OpenGL.GL_ENABLE_BIT | OpenGL.GL_EVAL_BIT);
            //gl.Enable(OpenGL.GL_AUTO_NORMAL);
            //gl.Enable(OpenGL.GL_NORMALIZE);
            //gl.Enable(OpenGL.GL_MAP2_VERTEX_3);
            //gl.Enable(OpenGL.GL_MAP2_TEXTURE_COORD_2);
            //gl.PushMatrix();
            //gl.Rotate(270.0f, 1.0f, 0.0f, 0.0f);
            //gl.Scale(0.5f * scale, 0.5f * scale, 0.5f * scale);
            //gl.Translate(0.0, 0.0, -1.5);
            foreach (var i in Enumerable.Range(-50, 100))
            {
                gl.Begin(BeginMode.Points);
                gl.Color(new GLColor(100, 1, 1, 1));
                gl.Vertex(i, i, i);
                gl.End();
            }
            ////Teapot tp = new Teapot();
            ////tp.Draw(gl, 14, 1, OpenGL.GL_FILL);

            //rotation += 3.0f;
            //gl.PopMatrix();
            //gl.PopAttrib();

            //gl.utSwapBuffers();
        }

        private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            //var gl = args.OpenGL;
            ////  Set the projection matrix.
            //gl.MatrixMode(OpenGL.GL_PROJECTION);

            ////  Load the identity.
            //gl.LoadIdentity();

            ////  Create a perspective transformation.
            //gl.Perspective(60.0f, (double)Width / (double)Height, 0.01, 100.0);

            ////  Use the 'look at' helper function to position and aim the camera.
            //gl.LookAt(-5, 5, -5, 0, 0, 0, 0, 1, 0);

            ////  Set the modelview matrix.
            //gl.MatrixMode(OpenGL.GL_MODELVIEW);
        }
        string fileName = null;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            
            {
                //openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "las files (*.las)|*.las|All files (*.las*)|*.las*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() ?? false)
                {
                    fileName = openFileDialog.FileName;
                }
            }
            
        }
    }
}
