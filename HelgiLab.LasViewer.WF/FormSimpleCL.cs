﻿using HelgiLab.Models;
using NLog;
using PanoTools.Panorama;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace HelgiLab
{
    public partial class FormSimpleCL : Form
    {
        readonly static Logger Log = LogManager.GetCurrentClassLogger();
        
        private FolderBrowserDialog folderBrowserDialog;
        LasViewerModel Model = null;
        public FormSimpleCL()
        {
            InitializeComponent();
            folderBrowserDialog = new FolderBrowserDialog();
        }
        public FormSimpleCL(string source, LasViewerModel model) : this()
        {
            this.Model = model;
            textBoxSourcePath.Text = source;
            UpdateDestinationDirectory();
            this.textBoxTo.Text = $"{this.Model.CameraPositions.Count - 1}";
        }
        private void UpdateDestinationDirectory()
        {
            var soursDir = textBoxSourcePath.Text.Split('\\').Last();
            textBoxDestinationPath.Text = $@"{Properties.Settings.Default.SimpleCLDestinationPath}\{getTrackNameForDisplay(soursDir)}";
        }
        private void buttonBrowseSource_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxSourcePath.Text = folderBrowserDialog.SelectedPath;
                UpdateDestinationDirectory();
            }
        }

        private void buttonBrowseDestination_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
                textBoxDestinationPath.Text = folderBrowserDialog.SelectedPath;
        }
        static string GetPrefix(string source)
        {
            string prefix = "posed_lb_pics";
            
            foreach (var f in Properties.Settings.Default.PanoramaFiles
                .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
            {
                string file = Path.Combine(source, f);
                if (!File.Exists(file)) continue;
                
                var lines = File.ReadLines(file).ToArray();
                var l = lines[1].Split(';', '\t', ',')[0].Split('\\', '/').Last().Split('.').First();
                prefix = "";
                while (!int.TryParse(l, out int num))
                {
                    prefix += l[0];
                    l = l.Substring(1);
                }
                break;
            }
            return prefix;
        }
        async private void buttonExecute_Click(object sender, EventArgs e)
        {
            int fl = 0;
            if (checkBoxDepth.Checked) fl |= 1;
            if (checkBoxImages.Checked) fl |= 2;
            if (!checkBoxEnchance.Checked) fl |= 8;
            int from = 0, to = 1;

            //startInfo.Arguments = "\"" + textBoxSourcePath.Text + "\" \"" + textBoxDestinationPath.Text + "\" -aaa -1 1 80 " + fl.ToString();
            var source = textBoxSourcePath.Text;
            var target = textBoxDestinationPath.Text;

            // создаем директорию
            if (!Directory.Exists(target))
                Directory.CreateDirectory(target);

            int.TryParse(textBoxFrom.Text, out from);
            int.TryParse(textBoxTo.Text, out to);

            var parTarget = Path.GetFileName(target);
            var prefix = GetPrefix(source);
            var startInfo = new ProcessStartInfo
            {
                FileName = Properties.Settings.Default.SimpleCLPath,
                WorkingDirectory = source,
                UseShellExecute = false,
                Arguments = $"\"{source}\" \"{target}\" {prefix} {from} {to} 85 {fl}"
            };
            
            var process = new Process { StartInfo = startInfo };
            
            try
            {
                if (process.Start())
                {
                    Log.Debug("process.Start " + process.StartInfo.Arguments);
                    DialogResult = DialogResult.OK;
                    // сохраним параметры процесса (после завеншения будут недоступны)
                    var p = new { process.ProcessName, process.Id, source, target };
                    // ожидаем завершения процесса
                    await Task.Run(() => process.WaitForExit());
                    // todo: анализ кода завершения SimpleCL
                    // публикация треков
                    PublishTrack(p.source, p.target, from, to);
                    PanoServerReload();
                    
                    Process.Start(Properties.Settings.Default.BrowserPath, $@"{Properties.Settings.Default.BrowserParam}&lang=en&panorama={parTarget}{HttpUtility.UrlEncode("\\")}{from.ToString("000000")}");
                }
                else
                {
                    throw new ApplicationException("Unable to run SimpleCL");
                }
            }
            catch(Exception ex)
            {
                Log.Debug(ex, "Unable to run SimpleCL");
                MessageBox.Show("Unable to run SimpleCL");
            }
        }
        void PanoServerReload()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Properties.Settings.Default.Panoserver + "/reload");

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                response.Close();
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "PanoServerReload");
            }
        }
        int GetTrackId(string trackName)
        {
            return -1;
        }
        private string getTrackNameForDisplay(string tname, int car = 20)
        {
            string[] parts = tname.Split('-');
            if (parts.Length < 5)
                return DateTime.Now.ToString("ddMMyy-HHmm-20");
            else
                return parts[4] + parts[3] + parts[2].Substring(2) + '-' + parts[5] + parts[6] + "-" + car.ToString("00");
        }

        private int PublishTrack(string source, string target, int from = 0, int to = 1)
        {
            var connectionString = ConfigurationManager
                .ConnectionStrings["bd_proccessing_connection"]
                .ConnectionString
                ;

            var ii = NumberFormatInfo.InvariantInfo;
            
            var panos = new PositionEn(Model.CameraPosPath, Model.IsIPS3)
                    .ToEnumerable()
                    .Skip(from)
                    .TakeWhile((x, i) => to == 1 || i < to - from)
                    .Select(_ => new {time = _.GpsTimeS, lat = _.Y, lon = _.X, z = _.Z })
                    .ToList()
                ;

            var track_name = target.Split('\\').Last();
            var track_id = GetTrackId(track_name);
            var first = -1;

            using (Npgsql.NpgsqlConnection conn = new Npgsql.NpgsqlConnection(connectionString))
            {
                conn.Open();
                using (var transaction = conn.BeginTransaction())
                {
                    try
                    {
                        using (Npgsql.NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;

                            var datetime = track_name.Substring(0, 4) + track_name.Substring(6, 5);
                            var year = track_name.Substring(4, 2);
                            var car = int.Parse(track_name.Substring(12, 2));

                            cmd.CommandText = $"select iid from panoramas.track_info where datekey='{datetime}' and year={year} and car={car};";
                            var res = cmd.ExecuteScalar();
                            track_id = res == null ? -1 : Convert.ToInt32(res);

                            if (track_id == -1)
                            {
                                cmd.CommandText = $"insert into panoramas.track_info(datekey, year, car, first, path, season_id) values ('{datetime}',{year},{car},-1,'{target}',1) RETURNING iid;";
                                track_id = Convert.ToInt32(cmd.ExecuteScalar());

                                cmd.CommandText = $"insert into panoramas.track_group (group_id, track_id) values (1, {track_id})";
                                cmd.ExecuteNonQuery();
                            }

                            cmd.CommandText = $"delete from panoramas.panoramas where track_id={track_id}";
                            cmd.ExecuteNonQuery();

                            foreach (var p in panos)
                            {
                                cmd.CommandText = $"insert into panoramas.panoramas (lat,lon,z,time,track_id) values ({p.lat.ToString(ii)},{p.lon.ToString(ii)},{p.z.ToString(ii)},{(long)p.time},{track_id}) RETURNING iid;";
                                if (first == -1)
                                    first = Convert.ToInt32(cmd.ExecuteScalar());
                                else
                                    cmd.ExecuteNonQuery();
                            }

                            cmd.CommandText = $@"
                                update panoramas.panoramas set scale=10 where scale is null and (iid&1023) = 0;
                                update panoramas.panoramas set scale=9 where scale is null and (iid&511) = 0;
                                update panoramas.panoramas set scale=8 where scale is null and (iid&255) = 0;
                                update panoramas.panoramas set scale=7 where scale is null and (iid&127) = 0;
                                update panoramas.panoramas set scale=6 where scale is null and (iid&63) = 0;
                                update panoramas.panoramas set scale=5 where scale is null and (iid&31) = 0;
                                update panoramas.panoramas set scale=4 where scale is null and (iid&15) = 0;
                                update panoramas.panoramas set scale=3 where scale is null and (iid&7) = 0;
                                update panoramas.panoramas set scale=2 where scale is null and (iid&3) = 0;
                                update panoramas.panoramas set scale=1 where scale is null and (iid&1) = 0;
                                update panoramas.panoramas set scale=0 where scale is null;
                                update panoramas.track_info set first = {first} where iid = {track_id}";

                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    return track_id;
                }
            }
        }
    }
}
