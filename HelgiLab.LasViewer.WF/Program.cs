﻿using System;
using System.Windows.Forms;

namespace HelgiLab
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //var form = new MainForm { Text = $"HelgiLab Las Viewer  - V {Application.ProductVersion}"};
                var form = new MainForm { Text = $"Las Builder  - V {Application.ProductVersion}" };
                Application.Run(form);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
