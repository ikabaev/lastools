﻿namespace HelgiLab
{
    partial class PointsNode
    {

        private const string randomVert = @"#version 150 core

in vec3 inPosition;
in vec3 inColor;

uniform mat4 mvpMatrix;
uniform bool enableRendering = true;
uniform bool realColor = true;
uniform vec3 oneColor;
uniform int pointSize = 1;


out vec3 passColor;

void main(void) {
    // transform vertex' position from model space to clip space.
    gl_Position = mvpMatrix * vec4(inPosition, 1.0);
    gl_PointSize = (3 * pointSize * pointSize + 10) / (/*gl_Position.z **/ gl_Position.z + 1);
    
    if (!enableRendering)
        return;

    if(oneColor.x + oneColor.y + oneColor.z != 0)
        passColor = oneColor;

    if (realColor)
        passColor = inColor;
}

";
        private const string randomFrag = @"#version 150

in vec3 passColor;

out vec4 out_Color;

void main(void) {
	//out_Color = vec4(passColor.x / 65536, passColor.y / 65536, passColor.z / 65536, 1.0);
    out_Color = vec4(passColor, 1.0);
}
";
        private const string gl_VertexIDVert = @"// vertex shader that gets color value according to gl_VertexID.
#version 150 core

in vec3 inPosition;
out vec4 passColor;
uniform mat4 mvpMatrix;

void main(void) {
    // transform vertex' position from model space to clip space.
    gl_Position = mvpMatrix * vec4(inPosition, 1.0);


    // gets color value according to gl_VertexID.
    int index = gl_VertexID;
    passColor = vec4(
        float(index & 0xFF) / 255.0, 
        float((index >> 8) & 0xFF) / 255.0, 
        float((index >> 16) & 0xFF) / 255.0, 
        float((index >> 24) & 0xFF) / 255.0);
}
";
        private const string gl_VertexIDFrag = @"#version 150

in vec4 passColor;

out vec4 out_Color;

void main(void) {
	out_Color = passColor;
}
";

    }
}
