﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelgiLab
{
    public enum PointsColor
    {
        /// <summary>
        /// Реальный цвет
        /// </summary>
        Real,
        /// <summary>
        /// Установленный цвет для каждого радара
        /// </summary>
        BySource
    }
}
