﻿using CSharpGL;
using PanoTools.Panorama;

namespace HelgiLab
{
    partial class SphereNode : ModernNode/*PickableNode*/, IRenderable
    {
        private PolygonModeSwitch polygonMode = new PolygonModeSwitch(PolygonMode.Line);

        private const string tex = "tex";
        private const string inUV = "inUV";
        public static SphereNode Create()
        {
            var model = new SphereModel(1000, 51, 51);
            var vs = new VertexShader(regularVert);
            var fs = new FragmentShader(regularFrag);
            var array = new ShaderArray(vs, fs);

            var map = new AttributeMap();
            map.Add("inPosition", SphereModel.strPosition);
            //map.Add("inColor", SphereModel.strColor);
            map.Add(inUV, SphereModel.strUV);
            var builder = new RenderMethodBuilder(array, map);
            var node = new SphereNode(model, SphereModel.strPosition, builder);
            
            node.Initialize();

            return node;
        }
        public bool Show = true;
        private SphereNode(IBufferSource model, string positionNameInIBufferSource, params RenderMethodBuilder[] builders)
            : base(model, /*positionNameInIBufferSource,*/ builders)
        {
        }

        CameraPosition _cameraPosition = null;
        mat4 _rt = glm.rotate((float)180, new vec3(0, 1, 0));
        public CameraPosition CameraPosition
        {
            get => _cameraPosition;
            set
            {
                //_cameraPosition = value;
                //Quaternion = PanoTools.Quaternion.CreateFromYawPitchRoll(
                //    -_cameraPosition.AttitudeY * Math.PI / 180,
                //    _cameraPosition.AttitudeZ * Math.PI / 180 + Math.PI,
                //    Math.PI - _cameraPosition.AttitudeX * Math.PI / 180);

                //var m = Quaternion.ToMatrix();
                //vec4 col0 = new vec4((float)m.M11, (float)m.M21, (float)m.M31, (float)m.M41);
                //vec4 col1 = new vec4((float)m.M12, (float)m.M22, (float)m.M32, (float)m.M42);
                //vec4 col2 = new vec4((float)m.M13, (float)m.M23, (float)m.M33, (float)m.M43);
                //vec4 col3 = new vec4((float)m.M14, (float)m.M24, (float)m.M34, (float)m.M44);

                //_rt = new mat4(col0, col1, col2, col3);

                //var rt = glm.rotate((float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));
                //rt = glm.rotate(rt, (float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                //rt = glm.rotate(rt, (float)(-CameraPosition.AttitudeY), new vec3(1, 0, 0));


                if (_cameraPosition != null)
                {
                    //var rt = glm.rotate((float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                    //rt = glm.rotate(rt, (float)(CameraPosition.AttitudeY), new vec3(1, 0, 0));
                    //rt = glm.rotate(rt, (float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));

                    
                    
                    var rt = glm.rotate((float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                    rt = glm.rotate(rt, (float)CameraPosition.AttitudeZ -90, new vec3(0, 1, 0));
                    rt = glm.rotate(_rt, (float)(CameraPosition.AttitudeY), new vec3(1, 0, 0));

                    _rt = rt;
                }

            }
        }
        public ITextureSource TextureSource { get => _tex; set => _tex = value; }
        #region IRenderable 成员

        public ThreeFlags EnableRendering { get { return ThreeFlags.BeforeChildren | ThreeFlags.Children; } set { } }
        //public ThreeFlags EnableRendering { get; set; } = ThreeFlags.BeforeChildren;

        ITextureSource _tex;
        public void RenderBeforeChildren(RenderEventArgs arg)
        {
            ICamera camera = arg.Camera;
            mat4 projection = camera.GetProjectionMatrix();
            mat4 view = camera.GetViewMatrix();
            mat4 model = this.GetModelMatrix();

            RenderMethod method = this.RenderUnit.Methods[0];
            ShaderProgram program = method.Program;
            program.SetUniform("mvpMat", projection * view * /*_rt **/ model);
            //if(_cameraPosition == null)
            //    program.SetUniform("mvpMat", projection * view * model);
            //else
            //    program.SetUniform("mvpMat", projection * view * _rt * model);

            //if (TextureSource == null)
            //    TextureSource = GetTexture();
            //var _tex = (TextureSource)TextureSource; 
            //if (_tex != null)
            //    program.SetUniform(tex, new TextureSource(_tex._filename).BindingTexture);

            // не пойму почему program.SetUniform(tex, TextureSource.BindingTexture) не работает;
            //if (Show && TextureSource != null && _tex?._filename != (this.TextureSource as TextureSource)?._filename)
            //{
            //    _tex = new TextureSource(((TextureSource)TextureSource)._filename);
            //}
            //else if (TextureSource == null)
            //{
            //    _tex = null;
            //}
            if (_tex != null)
                program.SetUniform(tex, _tex?.BindingTexture);
            

            //this.polygonMode.On();
            if (Show)
                method.Render();
            //this.polygonMode.Off();
        }

        public void RenderAfterChildren(RenderEventArgs arg)
        {
        }

        #endregion

    }
}
