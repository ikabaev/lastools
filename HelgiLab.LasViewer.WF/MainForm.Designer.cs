﻿namespace HelgiLab
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSimpleCL = new System.Windows.Forms.ToolStripButton();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPagePoints = new System.Windows.Forms.TabPage();
            this.winGLCanvasPoints = new CSharpGL.WinGLCanvas();
            this.tabPagePhoto = new System.Windows.Forms.TabPage();
            this.panelPhoto = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxPointsShow = new System.Windows.Forms.CheckBox();
            this.lblColor1 = new System.Windows.Forms.Label();
            this.lblColor2 = new System.Windows.Forms.Label();
            this.lblColor3 = new System.Windows.Forms.Label();
            this.lblColor4 = new System.Windows.Forms.Label();
            this.comboBoxPointsColor = new System.Windows.Forms.ComboBox();
            this.lblColor5 = new System.Windows.Forms.Label();
            this.comboBoxPointSize = new System.Windows.Forms.ComboBox();
            this.chB1 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.chB2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panelShowPano = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonRotationApply = new System.Windows.Forms.Button();
            this.checkBoxPanoShow = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxRotateZ = new System.Windows.Forms.TextBox();
            this.textBoxRotateX = new System.Windows.Forms.TextBox();
            this.textBoxRotateY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonBackToCameraPosition = new System.Windows.Forms.Button();
            this.groupBoxViewArea = new System.Windows.Forms.GroupBox();
            this.radioButtonLas = new System.Windows.Forms.RadioButton();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.radioButtonPano = new System.Windows.Forms.RadioButton();
            this.comboBoxLasTimes = new System.Windows.Forms.ComboBox();
            this.labelBeginTime2 = new System.Windows.Forms.Label();
            this.labelCount2 = new System.Windows.Forms.Label();
            this.labelfileName2 = new System.Windows.Forms.Label();
            this.labelBeginTime = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.Label();
            this.panelViewLas = new System.Windows.Forms.Panel();
            this.progressBarReadLassPB = new System.Windows.Forms.ProgressBar();
            this.labelPointCount = new System.Windows.Forms.Label();
            this.buttonSeek = new System.Windows.Forms.Button();
            this.comboBoxPointsCount = new System.Windows.Forms.ComboBox();
            this.labelfileName = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPagePoints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.winGLCanvasPoints)).BeginInit();
            this.tabPagePhoto.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelShowPano.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxViewArea.SuspendLayout();
            this.panelViewLas.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.printToolStripButton,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripButton,
            this.toolStripButtonSimpleCL});
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Name = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.newToolStripButton, "newToolStripButton");
            this.newToolStripButton.Name = "newToolStripButton";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.openToolStripButton, "openToolStripButton");
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.saveToolStripButton, "saveToolStripButton");
            this.saveToolStripButton.Name = "saveToolStripButton";
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.printToolStripButton, "printToolStripButton");
            this.printToolStripButton.Name = "printToolStripButton";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            resources.ApplyResources(this.toolStripSeparator, "toolStripSeparator");
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.cutToolStripButton, "cutToolStripButton");
            this.cutToolStripButton.Name = "cutToolStripButton";
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.copyToolStripButton, "copyToolStripButton");
            this.copyToolStripButton.Name = "copyToolStripButton";
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.pasteToolStripButton, "pasteToolStripButton");
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.helpToolStripButton, "helpToolStripButton");
            this.helpToolStripButton.Name = "helpToolStripButton";
            // 
            // toolStripButtonSimpleCL
            // 
            this.toolStripButtonSimpleCL.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSimpleCL.Image = global::HelgiLab.Properties.Resources.desktop;
            resources.ApplyResources(this.toolStripButtonSimpleCL, "toolStripButtonSimpleCL");
            this.toolStripButtonSimpleCL.Name = "toolStripButtonSimpleCL";
            this.toolStripButtonSimpleCL.Click += new System.EventHandler(this.toolStripButtonSimpleCL_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPagePoints);
            this.tabControl1.Controls.Add(this.tabPagePhoto);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabPagePoints
            // 
            this.tabPagePoints.Controls.Add(this.winGLCanvasPoints);
            resources.ApplyResources(this.tabPagePoints, "tabPagePoints");
            this.tabPagePoints.Name = "tabPagePoints";
            this.tabPagePoints.UseVisualStyleBackColor = true;
            // 
            // winGLCanvasPoints
            // 
            this.winGLCanvasPoints.AccumAlphaBits = ((byte)(0));
            this.winGLCanvasPoints.AccumBits = ((byte)(0));
            this.winGLCanvasPoints.AccumBlueBits = ((byte)(0));
            this.winGLCanvasPoints.AccumGreenBits = ((byte)(0));
            this.winGLCanvasPoints.AccumRedBits = ((byte)(0));
            this.winGLCanvasPoints.BackColor = System.Drawing.SystemColors.ActiveCaption;
            resources.ApplyResources(this.winGLCanvasPoints, "winGLCanvasPoints");
            this.winGLCanvasPoints.Name = "winGLCanvasPoints";
            this.winGLCanvasPoints.RenderTrigger = CSharpGL.RenderTrigger.TimerBased;
            this.winGLCanvasPoints.StencilBits = ((byte)(0));
            this.winGLCanvasPoints.TimerTriggerInterval = 40;
            this.winGLCanvasPoints.UpdateContextVersion = true;
            this.winGLCanvasPoints.OpenGLDraw += new CSharpGL.GLEventHandler<System.Windows.Forms.PaintEventArgs>(this.winGLCanvasPoints_OpenGLDraw);
            this.winGLCanvasPoints.MouseMove += new System.Windows.Forms.MouseEventHandler(this.winGLCanvasPoints_MouseMove);
            // 
            // tabPagePhoto
            // 
            this.tabPagePhoto.Controls.Add(this.panelPhoto);
            resources.ApplyResources(this.tabPagePhoto, "tabPagePhoto");
            this.tabPagePhoto.Name = "tabPagePhoto";
            this.tabPagePhoto.UseVisualStyleBackColor = true;
            // 
            // panelPhoto
            // 
            resources.ApplyResources(this.panelPhoto, "panelPhoto");
            this.panelPhoto.Name = "panelPhoto";
            this.panelPhoto.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPhoto_Paint);
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.groupBox2);
            this.panelControls.Controls.Add(this.panelShowPano);
            this.panelControls.Controls.Add(this.groupBox3);
            this.panelControls.Controls.Add(this.groupBoxViewArea);
            this.panelControls.Controls.Add(this.labelBeginTime2);
            this.panelControls.Controls.Add(this.labelCount2);
            this.panelControls.Controls.Add(this.labelfileName2);
            this.panelControls.Controls.Add(this.labelBeginTime);
            this.panelControls.Controls.Add(this.labelCount);
            this.panelControls.Controls.Add(this.panelViewLas);
            this.panelControls.Controls.Add(this.labelfileName);
            resources.ApplyResources(this.panelControls, "panelControls");
            this.panelControls.Name = "panelControls";
            this.panelControls.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.checkBoxPointsShow);
            this.groupBox2.Controls.Add(this.lblColor1);
            this.groupBox2.Controls.Add(this.lblColor2);
            this.groupBox2.Controls.Add(this.lblColor3);
            this.groupBox2.Controls.Add(this.lblColor4);
            this.groupBox2.Controls.Add(this.comboBoxPointsColor);
            this.groupBox2.Controls.Add(this.lblColor5);
            this.groupBox2.Controls.Add(this.comboBoxPointSize);
            this.groupBox2.Controls.Add(this.chB1);
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.chB2);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.checkBox3);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // checkBoxPointsShow
            // 
            resources.ApplyResources(this.checkBoxPointsShow, "checkBoxPointsShow");
            this.checkBoxPointsShow.Checked = true;
            this.checkBoxPointsShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPointsShow.Name = "checkBoxPointsShow";
            this.checkBoxPointsShow.UseVisualStyleBackColor = true;
            this.checkBoxPointsShow.CheckedChanged += new System.EventHandler(this.checkBoxPanoShow_CheckedChanged);
            // 
            // lblColor1
            // 
            this.lblColor1.BackColor = System.Drawing.Color.MediumBlue;
            resources.ApplyResources(this.lblColor1, "lblColor1");
            this.lblColor1.Name = "lblColor1";
            this.lblColor1.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor2
            // 
            this.lblColor2.BackColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.lblColor2, "lblColor2");
            this.lblColor2.Name = "lblColor2";
            this.lblColor2.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor3
            // 
            this.lblColor3.BackColor = System.Drawing.Color.YellowGreen;
            resources.ApplyResources(this.lblColor3, "lblColor3");
            this.lblColor3.Name = "lblColor3";
            this.lblColor3.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor4
            // 
            this.lblColor4.BackColor = System.Drawing.Color.DarkGoldenrod;
            resources.ApplyResources(this.lblColor4, "lblColor4");
            this.lblColor4.Name = "lblColor4";
            this.lblColor4.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // comboBoxPointsColor
            // 
            this.comboBoxPointsColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointsColor.FormattingEnabled = true;
            this.comboBoxPointsColor.Items.AddRange(new object[] {
            resources.GetString("comboBoxPointsColor.Items"),
            resources.GetString("comboBoxPointsColor.Items1")});
            resources.ApplyResources(this.comboBoxPointsColor, "comboBoxPointsColor");
            this.comboBoxPointsColor.Name = "comboBoxPointsColor";
            this.comboBoxPointsColor.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPointsColor_SelectedIndexChanged);
            // 
            // lblColor5
            // 
            this.lblColor5.BackColor = System.Drawing.Color.DarkCyan;
            resources.ApplyResources(this.lblColor5, "lblColor5");
            this.lblColor5.Name = "lblColor5";
            this.lblColor5.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // comboBoxPointSize
            // 
            this.comboBoxPointSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointSize.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPointSize, "comboBoxPointSize");
            this.comboBoxPointSize.Name = "comboBoxPointSize";
            // 
            // chB1
            // 
            resources.ApplyResources(this.chB1, "chB1");
            this.chB1.Checked = true;
            this.chB1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chB1.Name = "chB1";
            this.chB1.UseVisualStyleBackColor = true;
            this.chB1.CheckedChanged += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // checkBox5
            // 
            resources.ApplyResources(this.checkBox5, "checkBox5");
            this.checkBox5.Checked = true;
            this.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // chB2
            // 
            resources.ApplyResources(this.chB2, "chB2");
            this.chB2.Checked = true;
            this.chB2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chB2.Name = "chB2";
            this.chB2.UseVisualStyleBackColor = true;
            this.chB2.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // checkBox4
            // 
            resources.ApplyResources(this.checkBox4, "checkBox4");
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBox3
            // 
            resources.ApplyResources(this.checkBox3, "checkBox3");
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // panelShowPano
            // 
            this.panelShowPano.Controls.Add(this.label4);
            this.panelShowPano.Controls.Add(this.buttonRotationApply);
            this.panelShowPano.Controls.Add(this.checkBoxPanoShow);
            this.panelShowPano.Controls.Add(this.label6);
            this.panelShowPano.Controls.Add(this.textBoxRotateZ);
            this.panelShowPano.Controls.Add(this.textBoxRotateX);
            this.panelShowPano.Controls.Add(this.textBoxRotateY);
            this.panelShowPano.Controls.Add(this.label5);
            resources.ApplyResources(this.panelShowPano, "panelShowPano");
            this.panelShowPano.Name = "panelShowPano";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // buttonRotationApply
            // 
            resources.ApplyResources(this.buttonRotationApply, "buttonRotationApply");
            this.buttonRotationApply.Name = "buttonRotationApply";
            this.buttonRotationApply.UseVisualStyleBackColor = true;
            this.buttonRotationApply.Click += new System.EventHandler(this.buttonRotationApply_Click);
            // 
            // checkBoxPanoShow
            // 
            resources.ApplyResources(this.checkBoxPanoShow, "checkBoxPanoShow");
            this.checkBoxPanoShow.Checked = true;
            this.checkBoxPanoShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPanoShow.Name = "checkBoxPanoShow";
            this.checkBoxPanoShow.UseVisualStyleBackColor = true;
            this.checkBoxPanoShow.CheckedChanged += new System.EventHandler(this.checkBoxPanoShow_CheckedChanged);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // textBoxRotateZ
            // 
            resources.ApplyResources(this.textBoxRotateZ, "textBoxRotateZ");
            this.textBoxRotateZ.Name = "textBoxRotateZ";
            // 
            // textBoxRotateX
            // 
            this.textBoxRotateX.AllowDrop = true;
            resources.ApplyResources(this.textBoxRotateX, "textBoxRotateX");
            this.textBoxRotateX.Name = "textBoxRotateX";
            // 
            // textBoxRotateY
            // 
            resources.ApplyResources(this.textBoxRotateY, "textBoxRotateY");
            this.textBoxRotateY.Name = "textBoxRotateY";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonBackToCameraPosition);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // buttonBackToCameraPosition
            // 
            resources.ApplyResources(this.buttonBackToCameraPosition, "buttonBackToCameraPosition");
            this.buttonBackToCameraPosition.Name = "buttonBackToCameraPosition";
            this.buttonBackToCameraPosition.UseVisualStyleBackColor = true;
            this.buttonBackToCameraPosition.Click += new System.EventHandler(this.buttonBackToCameraPosition_Click);
            // 
            // groupBoxViewArea
            // 
            this.groupBoxViewArea.Controls.Add(this.radioButtonLas);
            this.groupBoxViewArea.Controls.Add(this.buttonNext);
            this.groupBoxViewArea.Controls.Add(this.buttonPrev);
            this.groupBoxViewArea.Controls.Add(this.radioButtonPano);
            this.groupBoxViewArea.Controls.Add(this.comboBoxLasTimes);
            resources.ApplyResources(this.groupBoxViewArea, "groupBoxViewArea");
            this.groupBoxViewArea.Name = "groupBoxViewArea";
            this.groupBoxViewArea.TabStop = false;
            // 
            // radioButtonLas
            // 
            resources.ApplyResources(this.radioButtonLas, "radioButtonLas");
            this.radioButtonLas.Name = "radioButtonLas";
            this.radioButtonLas.UseVisualStyleBackColor = true;
            this.radioButtonLas.CheckedChanged += new System.EventHandler(this.radioButtonViewArea_CheckedChanged);
            // 
            // buttonNext
            // 
            resources.ApplyResources(this.buttonNext, "buttonNext");
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            resources.ApplyResources(this.buttonPrev, "buttonPrev");
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // radioButtonPano
            // 
            resources.ApplyResources(this.radioButtonPano, "radioButtonPano");
            this.radioButtonPano.Checked = true;
            this.radioButtonPano.Name = "radioButtonPano";
            this.radioButtonPano.TabStop = true;
            this.radioButtonPano.UseVisualStyleBackColor = true;
            this.radioButtonPano.CheckedChanged += new System.EventHandler(this.radioButtonViewArea_CheckedChanged);
            // 
            // comboBoxLasTimes
            // 
            this.comboBoxLasTimes.DisplayMember = "\"yyyy MM dd HH mm ss\"";
            this.comboBoxLasTimes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLasTimes.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxLasTimes, "comboBoxLasTimes");
            this.comboBoxLasTimes.Name = "comboBoxLasTimes";
            // 
            // labelBeginTime2
            // 
            resources.ApplyResources(this.labelBeginTime2, "labelBeginTime2");
            this.labelBeginTime2.Name = "labelBeginTime2";
            this.labelBeginTime2.Tag = "";
            // 
            // labelCount2
            // 
            resources.ApplyResources(this.labelCount2, "labelCount2");
            this.labelCount2.Name = "labelCount2";
            this.labelCount2.Tag = "";
            // 
            // labelfileName2
            // 
            resources.ApplyResources(this.labelfileName2, "labelfileName2");
            this.labelfileName2.Name = "labelfileName2";
            this.labelfileName2.Tag = "";
            // 
            // labelBeginTime
            // 
            resources.ApplyResources(this.labelBeginTime, "labelBeginTime");
            this.labelBeginTime.Name = "labelBeginTime";
            this.labelBeginTime.Tag = "";
            // 
            // labelCount
            // 
            resources.ApplyResources(this.labelCount, "labelCount");
            this.labelCount.Name = "labelCount";
            this.labelCount.Tag = "";
            // 
            // panelViewLas
            // 
            this.panelViewLas.Controls.Add(this.progressBarReadLassPB);
            this.panelViewLas.Controls.Add(this.labelPointCount);
            this.panelViewLas.Controls.Add(this.buttonSeek);
            this.panelViewLas.Controls.Add(this.comboBoxPointsCount);
            resources.ApplyResources(this.panelViewLas, "panelViewLas");
            this.panelViewLas.Name = "panelViewLas";
            // 
            // progressBarReadLassPB
            // 
            resources.ApplyResources(this.progressBarReadLassPB, "progressBarReadLassPB");
            this.progressBarReadLassPB.Name = "progressBarReadLassPB";
            // 
            // labelPointCount
            // 
            resources.ApplyResources(this.labelPointCount, "labelPointCount");
            this.labelPointCount.Name = "labelPointCount";
            this.labelPointCount.Tag = "";
            // 
            // buttonSeek
            // 
            resources.ApplyResources(this.buttonSeek, "buttonSeek");
            this.buttonSeek.Name = "buttonSeek";
            this.buttonSeek.UseVisualStyleBackColor = true;
            this.buttonSeek.Click += new System.EventHandler(this.buttonSeek_Click);
            // 
            // comboBoxPointsCount
            // 
            this.comboBoxPointsCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointsCount.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxPointsCount, "comboBoxPointsCount");
            this.comboBoxPointsCount.Name = "comboBoxPointsCount";
            // 
            // labelfileName
            // 
            resources.ApplyResources(this.labelfileName, "labelfileName");
            this.labelfileName.Name = "labelfileName";
            this.labelfileName.Tag = "";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPagePoints.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.winGLCanvasPoints)).EndInit();
            this.tabPagePhoto.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelShowPano.ResumeLayout(false);
            this.panelShowPano.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBoxViewArea.ResumeLayout(false);
            this.groupBoxViewArea.PerformLayout();
            this.panelViewLas.ResumeLayout(false);
            this.panelViewLas.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPagePoints;
        private System.Windows.Forms.Button buttonSeek;
        private CSharpGL.WinGLCanvas winGLCanvasPoints;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox chB2;
        private System.Windows.Forms.CheckBox chB1;
        private System.Windows.Forms.Label lblColor5;
        private System.Windows.Forms.Label lblColor4;
        private System.Windows.Forms.Label lblColor3;
        private System.Windows.Forms.Label lblColor2;
        private System.Windows.Forms.Label lblColor1;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.ProgressBar progressBarReadLassPB;
        private System.Windows.Forms.ComboBox comboBoxLasTimes;
        private System.Windows.Forms.TabPage tabPagePhoto;
        private System.Windows.Forms.Panel panelPhoto;
        private System.Windows.Forms.ComboBox comboBoxPointSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRotateZ;
        private System.Windows.Forms.TextBox textBoxRotateY;
        private System.Windows.Forms.TextBox textBoxRotateX;
        private System.Windows.Forms.Button buttonRotationApply;
        private System.Windows.Forms.CheckBox checkBoxPanoShow;
        private System.Windows.Forms.Button buttonBackToCameraPosition;
        private System.Windows.Forms.ComboBox comboBoxPointsColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelBeginTime;
        private System.Windows.Forms.GroupBox groupBoxViewArea;
        private System.Windows.Forms.RadioButton radioButtonLas;
        private System.Windows.Forms.RadioButton radioButtonPano;
        private System.Windows.Forms.ComboBox comboBoxPointsCount;
        private System.Windows.Forms.Label labelPointCount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxPointsShow;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panelViewLas;
        private System.Windows.Forms.Panel panelShowPano;
        private System.Windows.Forms.ToolStripButton toolStripButtonSimpleCL;
        private System.Windows.Forms.Label labelBeginTime2;
        private System.Windows.Forms.Label labelCount2;
        private System.Windows.Forms.Label labelfileName2;
        private System.Windows.Forms.Label labelfileName;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
    }
}

