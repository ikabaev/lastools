﻿using CSharpGL;
using HelgiLab.Models;
using PanoTools;
using PanoTools.Las;
using PanoTools.Panorama;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace HelgiLab
{
    public partial class MainForm : Form
    {
        delegate void FuncType<T>(T value);

        private Scene scene/*, scene3d*/;
        private ActionList actionList/*, actionList3d*/;
        private Camera camera/*, camera3d*/;
        private Picking pickingAction;
        //private SceneNodeBase _rootNode = null;

        public MainForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Параметры камеры в некоторой позиции
        /// </summary>
        CameraPosition CameraPosition => comboBoxLasTimes.SelectedValue as CameraPosition;
        DateTime? TimePosition => (comboBoxLasTimes.SelectedValue as DateTime?)?.AddHours(-3);

        //vec3? first = null;
        int _pointSize = 2;
        //vec4? _quaternion = null;
        //vec3? _shift = null;


        private SceneNodeBase GetRootNode(string lasPath, int seek = 0)
        {
            //IntPtr transf = CAPI.createTransform(3);
            Dictionary<ulong, List<vec3[]>> pointsDict = new Dictionary<ulong, List<vec3[]>>();
            List<vec3[]> vects = new List<vec3[]>();
            //first = null;
            FuncType<int> dlg = value => { progressBarReadLassPB.Value = value; Application.DoEvents(); };
            PointLatLonConverter conv;
            using (var las = new LasEn(lasPath))
            {
                // центральная точка
                las.Seek(seek);
                var centerPoint = las.Current;

                // показываем точек
                pointCount = (int)comboBoxPointsCount.SelectedValue;

                // точка начала интервала
                seek -= pointCount / 2;
                if (seek < 1)
                    seek = 1;

                las.Seek(seek);

                var numP = las.GetNumberOfPoints();
                
                if (seek > numP)
                {
                    vects.Clear();
                }

                progressBarReadLassPB.Minimum = 0;
                progressBarReadLassPB.Maximum = 100;
                var step = pointCount / 100;
                int pbVal = 0;
                progressBarReadLassPB.Value = pbVal;

                if (CameraPosition != null)
                {
                    var cPos = CameraPosition;
                    var cp = cPos.Clone();
                    centerPoint = new LasPoint { X = cp.X, Y = cp.Y, Z = cp.Z };
                }

                conv = new PointLatLonConverter(centerPoint.X, centerPoint.Y, centerPoint.Z);
                conv.ConvertFields(centerPoint.X, centerPoint.Y, centerPoint.Z, out centerPoint.X, out centerPoint.Y, out centerPoint.Z);

                #region ЧИТАЕМ ЛАС
                int i = 0;
                while (las.MoveNext() && i++ < pointCount)
                {
                    var c1 = las.Current;

                    if (c1.X < 100)
                        conv.ConvertFields(c1.X, c1.Y, c1.Z, out c1.X, out c1.Y, out c1.Z);

                    if (centerPoint == null)
                    {
                        centerPoint = new LasPoint { X = c1.X, Y = c1.Y, Z = c1.Z };
                    }

                    if (!pointsDict.ContainsKey(c1.PointSourceID))
                    {
                        pointsDict.Add(c1.PointSourceID, vects = new List<vec3[]>());
                    }
                    vects = pointsDict[c1.PointSourceID];
                    vects.Add(new[] { new vec3((float)(c1.X), (float)(c1.Z), -(float)(c1.Y)), new vec3(c1.R, c1.G, c1.B) });

                    if (Math.Floor((double)i / step) != pbVal)
                    {
                        pbVal = (int)Math.Floor((double)i / step);

                        progressBarReadLassPB.Invoke(dlg, pbVal);
                    }
                }
                #endregion
            }

            var group = this.scene.RootNode;

            int colorI = 1;
            group.Children.Clear();
            foreach (var pd in pointsDict.OrderBy(_ => _.Key))
            {
                var pos = pd.Value.Select(_ => _[0]).ToArray();
                var colors = pd.Value.Select(_ => _[1] / 65536).ToArray();
                //var item = new CSharpGL.LegacyPointsNode { PointSize = _pointSize, VertexesColors = pd.Value, ShowVertexesColors = checkBoxShowSource.Checked, Color = GetRadarColor(colorI).ToVec3() };
                var item = PointsNode.Create(new PointsModel(pos, colors), PointsModel.strPosition, PointsModel.strColor);
                item.EnableRendering = ShowRadar(colorI) ? ThreeFlags.BeforeChildren | ThreeFlags.Children : ThreeFlags.None;
                item.Color = GetRadarColor(colorI).ToVec3();
                group.Children.Add(item);
                colorI++;
            }

            // все координаты камеры
            //var campos = Model.CameraPositions
            //    .Select(p => { conv.ConvertFields(p.X, p.Y, p.Z, out double x, out double y, out double z); return new { x, y, z }; })
            //    .Select(p => new vec3((float)p.x, (float)p.z, -(float)p.y))
            //    .ToList()
            //    ;

            //var camItem = new CSharpGL.LegacyPointsNode { PointSize = 18, Vertexes = campos, Color = Color.Red.ToVec3() };
            //group.Children.Add(camItem);


            return group;
        }      
        
        private void MainForm_Load(object sender, EventArgs e)
        {
            #region размер точек comboBoxPointSize
            comboBoxPointSize.DataSource = Enumerable.Range(1, 10).ToList();
            _pointSize = HelgiLab.Properties.Settings.Default.PointSize;
            
            comboBoxPointSize.SelectedIndex = _pointSize - 1;
            comboBoxPointSize.SelectedIndexChanged += new System.EventHandler(this.comboBoxPointSize_SelectedIndexChanged);
            #endregion

            #region цвет точек comboBoxPointsColor
            //comboBoxPointsColor.DataSource = new[]
            //{
            //    "Реальный",
            //    "Цвет источника"
            //}
            //.ToList();
            //comboBoxPointsColor.SelectedIndexChanged += ComboBoxPointsColor_SelectedIndexChanged;
            comboBoxPointsColor.SelectedIndex = 0;
            #endregion

            #region число точек comboBoxPointsCount
            comboBoxPointsCount.DataSource = LasViewerModel.PointsCounts;
            comboBoxPointsCount.DisplayMember = "Value";
            comboBoxPointsCount.ValueMember = "Key";
            comboBoxPointsCount.SelectedValue = Properties.Settings.Default.PointsCount;
            comboBoxPointsCount.SelectedIndexChanged += ComboBoxPointsCount_SelectedIndexChanged;
            #endregion
        }

        private void ComboBoxPointsColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.scene?.RootNode?.Children?.Count == null) return;

            var pointsColor = (PointsColor)comboBoxPointsColor.SelectedIndex;

            this.scene.RootNode
                    .Children
                    .OfType<PointsNode>()
                    .ToList()
                    .ForEach(_ => _.ShowRealColors = (pointsColor == PointsColor.Real))
                    ;
        }

        private void winGLCanvasPoints_OpenGLDraw(object sender, PaintEventArgs e)
        {
            try
            {
                var gl = GL.Instance;
                ActionList list = this.actionList;
                if (list != null)
                {
                    vec4 clearColor = this.scene.ClearColor;
                    GL.Instance.ClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
                    GL.Instance.Clear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT | GL.GL_STENCIL_BUFFER_BIT);

                    list.Act(new ActionParams(Viewport.GetCurrent()));
                }
                
            }
            catch { };
            
        }

        DateTime? _seekTime = null;
        
        LasViewerModel Model = new LasViewerModel();
        FirstPerspectiveManipulater manipulater;
        string LasFilePath = null;
        //List<CameraPosition> CamPositions = new List<CameraPosition>();
        /*async*/
        void UpdateBindings(LasViewerModel model = null)
        {
            model = model ?? this.Model;
            labelfileName2.Text = model.FileName;
            labelCount2.Text = model.PointsCount.ToString("### ### ### ###");
            labelBeginTime2.Text = model.BeginTime.ToString("yyyy/MM/dd HH:mm:ss");

            // проверяем наличие панорам
            radioButtonPano.Enabled = Model.HasPano;
            radioButtonLas.Checked = radioButtonLas.Checked || !Model.HasPano;
            panelShowPano.Enabled = Model.HasPano;

            //if (model.PositionType == PositionType.Pano)
            //    radioButtonPano.Checked = true;
            //else if (model.PositionType == PositionType.Las)
            //    radioButtonLas.Checked = true;

            radioButtonViewArea_CheckedChanged(null, null);

            //if (comboBoxCameraTime?.Items?.Count > 0)
            //{
            //    this.comboBoxCameraTime.SelectedIndexChanged -= this.comboBoxCameraTime_SelectedIndexChanged;
            //}
            //comboBoxCameraTime.DataSource = model.CameraPositions;
            ////comboBoxCameraTime.DisplayMember = "ToDisplay";
            //comboBoxCameraTime.SelectedIndexChanged += this.comboBoxCameraTime_SelectedIndexChanged;
        }
        private void radioButtonViewArea_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == null ||(sender as RadioButton).Checked)
            {
                this.comboBoxLasTimes.SelectedIndexChanged -= this.comboBoxLasTimes_SelectedIndexChanged;
                comboBoxLasTimes.FormatString = null;

                if (radioButtonPano.Checked)
                    comboBoxLasTimes.DataSource = Model.CameraPositions;
                else if (radioButtonLas.Checked)
                {
                    comboBoxLasTimes.DataSource = Model.LasTimes;
                    comboBoxLasTimes.FormatString = "yyyy.MM.dd HH:mm:ss";
                    
                }
                checkBoxPanoShow.Checked = radioButtonPano.Checked;
                comboBoxLasTimes.SelectedIndexChanged += this.comboBoxLasTimes_SelectedIndexChanged;


                //var sp = _rootNode as SphereNode;
                //if (sp != null)
                //    sp.TextureSource = null;
                ////this.scene.RootNode
                //if (radioButtonPano.Checked && _rootNode as SphereNode == null)
                //{
                //    _rootNode = SphereNode.Create();
                //}
                //else if (radioButtonLas.Checked && _rootNode as GroupNode == null)
                //{
                //    _rootNode = new GroupNode();
                //}
                //panelShowPano.Enabled = radioButtonPano.Checked; 

                //panelViewArea.Enabled = radioButtonLas.Checked;
            }
        }
        private void ComboBoxPointsCount_SelectedIndexChanged(object sender, EventArgs e)
        {
           // pointCount = Model.GetPointsCount(comboBoxPointsCount.SelectedIndex);
        }

        string _lasPath;
        /*async*/ private void openToolStripButton_Click(object sender, EventArgs e)
        {
            #region выбираем путь к лас файлу
            
            using (var openFileDialog = new FolderBrowserDialog())
            {
                //openFileDialog.InitialDirectory = "c:\\";
                //openFileDialog.Filter = "las files (*.las)|*.las|All files (*.las*)|*.las*";
                //openFileDialog.FilterIndex = 2;
                //openFileDialog.RestoreDirectory = true;
                if(_lasPath != null)
                    openFileDialog.SelectedPath = _lasPath;
               // openFileDialog.RootFolder = Environment.SpecialFolder.MyComputer;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _lasPath = openFileDialog.SelectedPath;
                }
            }
            if (_lasPath == null) return;
            LasFilePath = Path.Combine(_lasPath, "scans_WGS84_All.las");
            #endregion
            // включаем основную панель
            panelControls.Enabled = true;
            // загружаем модель
            Model.Load(LasFilePath);
            UpdateBindings();

            this.actionList = null;
            this.scene = null;
            if (comboBoxLasTimes.Items.Count > 10)
                comboBoxLasTimes.SelectedIndex = 9;
            else
                comboBoxLasTimes_SelectedIndexChanged(null, null);
        }


        int pointCount = 5000000;

        Color GetRadarColor(int num)
        {
            return
                num == 1 ? lblColor1.BackColor :
                num == 2 ? lblColor2.BackColor :
                num == 3 ? lblColor3.BackColor :
                num == 4 ? lblColor4.BackColor :
                num == 5 ? lblColor5.BackColor :
                Color.Empty;
        }
        bool ShowRadar(int num)
        {
            return
                num == 1 ? chB1.Checked :
                num == 2 ? chB2.Checked :
                num == 3 ? checkBox3.Checked :
                num == 4 ? checkBox4.Checked :
                num == 5 ? checkBox5.Checked :
                false;
        }
        private void lblColor_Click(object sender, EventArgs e)
        {
            Label l = sender as Label;
            if (l != null && this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                int num = int.Parse(l.Name.Substring(l.Name.Length - 1));
                Color color = this.colorDialog1.Color;
                l.BackColor = color;
                var radarNum = this.scene.RootNode
                    .Children
                    .Skip(num - 1)
                    .FirstOrDefault();

                if (radarNum != null)
                {
                    //var lpn = radarNum as LegacyPointsNode;
                    //lpn.ClearList();
                    var lpn = radarNum as PointsNode;
                    lpn.Color = color.ToVec3();
                }
            }
        }
        /// <summary>
        /// Показать/скрыть облако точек радара
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chB_CheckedChanged(object sender, EventArgs e)
        {
            var chB = sender as CheckBox;
            if (chB != null)
            {
                int num = int.Parse(chB.Name.Substring(chB.Name.Length - 1));
                var lpn = this.scene.RootNode
                    .Children
                    .Skip(num - 1)
                    .FirstOrDefault() as IRenderable;

                if (lpn != null)
                {
                    lpn.EnableRendering = chB.Checked ? ThreeFlags.BeforeChildren | ThreeFlags.Children : ThreeFlags.None;
                }
            }
        }

        ITextureSource TextureSource
        {
            get
            {
                var dirPath = Path.GetDirectoryName(LasFilePath);
                var photoFileName = CameraPosition.FileName;
                var photoPosPath = Path.Combine(dirPath, photoFileName);
                var ts = new TextureSource(photoPosPath);
                return ts;
            }
        }
        double RotateX
        {
            get => double.Parse(textBoxRotateX.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            set => textBoxRotateX.Text = value.ToString("##0.00", CultureInfo.InvariantCulture);
        }
        double RotateY
        {
            get => double.Parse(textBoxRotateY.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            set => textBoxRotateY.Text = value.ToString("##0.00", CultureInfo.InvariantCulture);
        }
        double RotateZ
        {
            get => double.Parse(textBoxRotateZ.Text.Replace(",", "."), CultureInfo.InvariantCulture);
            set => textBoxRotateZ.Text = value.ToString("##0.00", CultureInfo.InvariantCulture);
        }
        /// <summary>
        /// Изменение позиции в лас файле
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxLasTimes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var position = new vec3(0, 0, 0);
            var direction = new vec3(0, 0, -1);
            var up = new vec3(0, 1, 0);
            var target = position + direction/* new vec3(0, 0, -300)*/;

            if (!checkBoxPanoShow.Checked)
                checkBoxPanoShow.Checked = true;

            progressBarReadLassPB.Value = 0;
            #region scene

        var node = this.scene?.RootNode as SphereNode;
        if (node == null)
        {
            camera = new Camera(position, target, up, CameraType.Perspecitive, this.winGLCanvasPoints.Width, this.winGLCanvasPoints.Height);
                
            this.scene = new Scene(camera)
            {
                RootNode = node = SphereNode.Create(),
            };

            var tansformAction = new TransformAction(scene.RootNode);
            var renderAction = new RenderAction(scene);
            var actionList = new ActionList();
            actionList.Add(tansformAction); actionList.Add(renderAction);
            this.actionList = actionList;

            this.pickingAction = new Picking(scene);

            manipulater = new FirstPerspectiveManipulater() /*{ StepLength = 2f }*/;
            manipulater.Bind(camera, this.winGLCanvasPoints);
        }
        else
        {
            // изменилась понорама, удаляем старые точки
            node
                .Children
                .OfType<PointsNode>()
                .ToList()
                .ForEach(_ =>
                {
                    node.Children.Remove(_);
                })
            ;
        }

        node.TextureSource = null;

        if (CameraPosition != null)
        {
            var cp = CameraPosition;
            RotateX = cp.AttitudeX;
            RotateY = cp.AttitudeY;
            RotateZ = cp.AttitudeZ;

            _seekTime = cp.Time;
            var ts = TextureSource;
            node.TextureSource = ts;
        }
        else
        {
            _seekTime = TimePosition;
        }

        if (camera != null)
        {
            camera.Position = position;
            camera.Target = position + direction;
        }
        #endregion
            
            Application.DoEvents();
        }

        private void comboBoxPointSize_SelectedIndexChanged(object sender, EventArgs e)
        {

            _pointSize = comboBoxPointSize.SelectedIndex + 1;
            
            if (this.scene?.RootNode == null) return;

            foreach(var lpn in this.scene.RootNode.Children.OfType<PointsNode>())
            { 
                //lpn.ClearList();
                lpn.PointSize = _pointSize;
            }
        }

        private void buttonRotationApply_Click(object sender, EventArgs e)
        {
            
            Properties.Settings.Default.Reload();

            var cp = CameraPosition?.Clone();
            if (cp != null)
            {
                cp.AttitudeX = RotateX;
                cp.AttitudeY = RotateY;
                cp.AttitudeZ = RotateZ;
            }
            var node = this.scene?.RootNode as SphereNode;
            if (node == null) return;

            //node.CameraPosition = cp;
            node
                .Children
                .OfType<PointsNode>()
                .ToList()
                .ForEach(_ =>
                {
                   _.CameraPosition = cp;
                })
                ;
        }
        bool EnableRendering => checkBoxPanoShow.Checked;

        private void checkBoxPanoShow_CheckedChanged(object sender, EventArgs e)
        {
            var ch = sender as CheckBox;
            var node = this.scene?.RootNode as SphereNode;
            if (ch != null && node != null)
            {
                if (ch == checkBoxPanoShow)
                {
                    node.Show = ch.Checked && ch.Enabled;
                }
                else if (ch == checkBoxPointsShow)
                {
                    node
                        .Children
                        .OfType<PointsNode>()
                        .Select((el, i) => new { el, i})
                        .ToList()
                        .ForEach(_ =>
                        {
                            _.el.EnableRendering = ShowRadar(_.i+ 1) && ch.Checked ? ThreeFlags.BeforeChildren | ThreeFlags.BeforeChildren : ThreeFlags.None;
                        })
                        ;
                }
                
            }
            
        }

        private void buttonBackToCameraPosition_Click(object sender, EventArgs e)
        {
            // comboBoxCameraTime_SelectedIndexChanged(null, null);
            var cp = CameraPosition;
            RotateX = cp.AttitudeX;
            RotateY = cp.AttitudeY;
            RotateZ = cp.AttitudeZ;

            _seekTime = CameraPosition.Time;
            //buttonSeek.Text = CameraPosition.ToDisplay;

            var position = new vec3(0, 0, 0);
            var direction = new vec3(0, 0, -1);
            var up = new vec3(0, 1, 0);
            var target = position + direction/* new vec3(0, 0, -300)*/;
            if (camera != null)
            {
                camera.Position = position;
                camera.Target = position + direction;
            }
        }

        private void panelPhoto_Paint(object sender, PaintEventArgs e)
        {
            //CameraPosition = comboBoxCameraTime.SelectedValue as CameraPosition;
            if (CameraPosition == null) return;
            _seekTime = CameraPosition.Time;
            //buttonSeek.Text = _seekTime.ToString();

            var dirPath = Path.GetDirectoryName(LasFilePath);
            var photoFileName = CameraPosition.FileName;
            var photoPosPath = Path.Combine(dirPath, photoFileName);


            using (var graph = e.Graphics)
            {
                var bm = Bitmap.FromFile(photoPosPath);
                var gu = GraphicsUnit.Pixel;
                graph.DrawImage(bm, panelPhoto.Bounds, bm.GetBounds(ref gu), GraphicsUnit.Pixel);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.PointsCount = (int)comboBoxPointsCount.SelectedValue;
            Properties.Settings.Default.PointSize = comboBoxPointSize.SelectedIndex + 1;
            Properties.Settings.Default.Save();
        }

        private void buttonSeek_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(LasFilePath) || _seekTime == null) return;

            // находим позицию в ласе
            int? seek = Model.FindPosByTime(_seekTime.Value);

            var rootElement = GetRootNode(LasFilePath, seek.Value);


            if (this.scene != null)
            {
                this.scene.RootNode = rootElement;
            }
          
            // размер точки
            comboBoxPointSize_SelectedIndexChanged(null, null);
            // поворот и позиция камеры
            buttonRotationApply_Click(null, null);
            // показать панораму/точки
            checkBoxPanoShow_CheckedChanged(checkBoxPanoShow, null);
            checkBoxPanoShow_CheckedChanged(checkBoxPointsShow, null);
            // цвет точек
            ComboBoxPointsColor_SelectedIndexChanged(null, null);
        }

        private void toolStripButtonSimpleCL_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(LasFilePath))
                openToolStripButton_Click(sender, e);

            if (string.IsNullOrWhiteSpace(LasFilePath))
                return;

            var source = Path.GetDirectoryName(LasFilePath);
            using (var form = new FormSimpleCL(source, Model))
            {
                form.ShowDialog(this);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if(comboBoxLasTimes.SelectedIndex < comboBoxLasTimes.Items.Count - 1)
                comboBoxLasTimes.SelectedIndex++; 
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (comboBoxLasTimes.SelectedIndex > 0)
                comboBoxLasTimes.SelectedIndex--;
        }

        private void winGLCanvasPoints_MouseMove(object sender, MouseEventArgs e)
        {
            if (!winGLCanvasPoints.Focused)
                winGLCanvasPoints.Focus();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch( e.KeyCode)
            {
                case Keys.B:
                    buttonPrev_Click(sender, e);
                    break;
                case Keys.N:
                    buttonNext_Click(sender, e);
                    break;
                case Keys.Q:
                case Keys.W:
                case Keys.E:
                case Keys.A:
                case Keys.S:
                case Keys.D:
                    checkBoxPanoShow.Checked = false;
                    break;
            }
            
        }
    }
}
