﻿namespace HelgiLab
{
    partial class FormSimpleCL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxImages = new System.Windows.Forms.CheckBox();
            this.checkBoxDepth = new System.Windows.Forms.CheckBox();
            this.checkBoxEnchance = new System.Windows.Forms.CheckBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.buttonBrowseDestination = new System.Windows.Forms.Button();
            this.textBoxDestinationPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonBrowseSource = new System.Windows.Forms.Button();
            this.textBoxSourcePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkBoxImages
            // 
            this.checkBoxImages.AutoSize = true;
            this.checkBoxImages.Checked = true;
            this.checkBoxImages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxImages.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxImages.Location = new System.Drawing.Point(103, 103);
            this.checkBoxImages.Name = "checkBoxImages";
            this.checkBoxImages.Size = new System.Drawing.Size(99, 17);
            this.checkBoxImages.TabIndex = 19;
            this.checkBoxImages.Text = "process images";
            this.checkBoxImages.UseVisualStyleBackColor = true;
            // 
            // checkBoxDepth
            // 
            this.checkBoxDepth.AutoSize = true;
            this.checkBoxDepth.Checked = true;
            this.checkBoxDepth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDepth.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxDepth.Location = new System.Drawing.Point(103, 80);
            this.checkBoxDepth.Name = "checkBoxDepth";
            this.checkBoxDepth.Size = new System.Drawing.Size(128, 17);
            this.checkBoxDepth.TabIndex = 18;
            this.checkBoxDepth.Text = "generate depth buffer";
            this.checkBoxDepth.UseVisualStyleBackColor = true;
            // 
            // checkBoxEnchance
            // 
            this.checkBoxEnchance.AutoSize = true;
            this.checkBoxEnchance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkBoxEnchance.Location = new System.Drawing.Point(103, 57);
            this.checkBoxEnchance.Name = "checkBoxEnchance";
            this.checkBoxEnchance.Size = new System.Drawing.Size(100, 17);
            this.checkBoxEnchance.TabIndex = 17;
            this.checkBoxEnchance.Text = "enchance fotos";
            this.checkBoxEnchance.UseVisualStyleBackColor = true;
            // 
            // buttonExecute
            // 
            this.buttonExecute.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonExecute.Location = new System.Drawing.Point(411, 99);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(199, 23);
            this.buttonExecute.TabIndex = 1;
            this.buttonExecute.Text = "Execute";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // buttonBrowseDestination
            // 
            this.buttonBrowseDestination.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonBrowseDestination.Location = new System.Drawing.Point(535, 30);
            this.buttonBrowseDestination.Name = "buttonBrowseDestination";
            this.buttonBrowseDestination.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseDestination.TabIndex = 15;
            this.buttonBrowseDestination.Text = "Browse";
            this.buttonBrowseDestination.UseVisualStyleBackColor = true;
            this.buttonBrowseDestination.Click += new System.EventHandler(this.buttonBrowseDestination_Click);
            // 
            // textBoxDestinationPath
            // 
            this.textBoxDestinationPath.Location = new System.Drawing.Point(103, 32);
            this.textBoxDestinationPath.Name = "textBoxDestinationPath";
            this.textBoxDestinationPath.ReadOnly = true;
            this.textBoxDestinationPath.Size = new System.Drawing.Size(426, 20);
            this.textBoxDestinationPath.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(8, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Destination folder:";
            // 
            // buttonBrowseSource
            // 
            this.buttonBrowseSource.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonBrowseSource.Location = new System.Drawing.Point(535, 4);
            this.buttonBrowseSource.Name = "buttonBrowseSource";
            this.buttonBrowseSource.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseSource.TabIndex = 12;
            this.buttonBrowseSource.Text = "Browse";
            this.buttonBrowseSource.UseVisualStyleBackColor = true;
            this.buttonBrowseSource.Click += new System.EventHandler(this.buttonBrowseSource_Click);
            // 
            // textBoxSourcePath
            // 
            this.textBoxSourcePath.Location = new System.Drawing.Point(103, 6);
            this.textBoxSourcePath.Name = "textBoxSourcePath";
            this.textBoxSourcePath.Size = new System.Drawing.Size(426, 20);
            this.textBoxSourcePath.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(33, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Track folder:";
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Location = new System.Drawing.Point(312, 53);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(41, 20);
            this.textBoxFrom.TabIndex = 14;
            this.textBoxFrom.Text = "0";
            this.textBoxFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(381, 53);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(41, 20);
            this.textBoxTo.TabIndex = 14;
            this.textBoxTo.Text = "1";
            this.textBoxTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(246, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Track from";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(359, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "to";
            // 
            // FormSimpleCL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 130);
            this.Controls.Add(this.checkBoxImages);
            this.Controls.Add(this.checkBoxDepth);
            this.Controls.Add(this.checkBoxEnchance);
            this.Controls.Add(this.buttonExecute);
            this.Controls.Add(this.buttonBrowseDestination);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.textBoxFrom);
            this.Controls.Add(this.textBoxDestinationPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonBrowseSource);
            this.Controls.Add(this.textBoxSourcePath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSimpleCL";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Track options";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxImages;
        private System.Windows.Forms.CheckBox checkBoxDepth;
        private System.Windows.Forms.CheckBox checkBoxEnchance;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.Button buttonBrowseDestination;
        private System.Windows.Forms.TextBox textBoxDestinationPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonBrowseSource;
        private System.Windows.Forms.TextBox textBoxSourcePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}