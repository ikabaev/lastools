﻿using CSharpGL;
using System;
using System.Collections.Generic;

namespace HelgiLab
{
    internal class SphereModel : IBufferSource
    {
        internal vec3[] positions;
        internal vec3[] normals;
        internal vec3[] colors;
        internal vec2[] uv;
        internal uint[] _indexes;
        internal uint[] indexes
        {
            get
            { return _indexes; }
            set { _indexes = value; }
        }

        public const string strPosition = "position";
        private VertexBuffer positionBuffer;

        public const string strUV = "uv";
        private VertexBuffer uvBuffer;

        public const string strNormal = "normal";
        private VertexBuffer normalBuffer;

        public const string strColor = "color";
        private VertexBuffer colorBuffer;

        private IDrawCommand command;

        //private static Random random = new Random();

        //private static vec3 RandomVec3()
        //{
        //    return new vec3((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
        //}

        //private static readonly Func<int, int, vec3> defaultColorGenerator = new Func<int, int, vec3>(DefaultColorGenerator);

        //private static vec3 DefaultColorGenerator(int latitude, int longitude)
        //{
        //    return RandomVec3();
        //}

        /// <summary>
        ///
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="latitudeParts">用纬线把地球切割为几块。</param>
        /// <param name="longitudeParts">用经线把地球切割为几块。</param>
        /// <returns></returns>
        internal SphereModel(float radius = 1.0f, int latitudeParts = 10, int longitudeParts = 40)
        {
            if (radius <= 0.0f || latitudeParts < 2 || longitudeParts < 3) { throw new Exception(); }

            int vertexCount = (latitudeParts + 1) * (longitudeParts + 1);
            this.positions = new vec3[vertexCount];
            this.normals = new vec3[vertexCount];
            this.colors = new vec3[vertexCount];
            this.uv = new vec2[vertexCount];

            //int indexCount = (latitudeParts) * (2 * (longitudeParts + 1) + 1);
            //this.indexes = new uint[indexCount];

            int index = 0;

            //// 把星球平铺在一个平面上
            //for (int i = 0; i < latitudeParts + 1; i++)
            //{
            //    double theta = (latitudeParts - i * 2) * Math.PI / 2 / latitudeParts;
            //    double y = radius * Math.Sin(theta);
            //    for (int j = 0; j < longitudeParts + 1; j++)
            //    {
            //        double x = radius * (i - latitudeParts / 2);
            //        double z = radius * (j - longitudeParts / 2);

            //        vec3 position = new vec3((float)x, (float)y, (float)z);
            //        this.positions[index] = position;

            //        this.normals[index] = position.normalize();

            //        this.colors[index] = colorGenerator(i, j);

            //        index++;
            //    }
            //}

            for (int i = 0; i < latitudeParts + 1; i++)
            {
                double theta = (latitudeParts - i * 2) * Math.PI / 2.0 / latitudeParts;
                double y = radius * Math.Sin(theta);

                for (int j = 0; j < longitudeParts + 1; j++)
                {
                    double x = -radius * Math.Cos(theta) * Math.Sin(j * Math.PI * 2.0 / longitudeParts);
                    double z = -radius * Math.Cos(theta) * Math.Cos(j * Math.PI * 2.0 / longitudeParts);

                    vec3 position = new vec3((float)x, (float)y, (float)z);
                    this.positions[index] = position;

                    this.normals[index] = position.normalize();

                    vec3 color = position.normalize();
                    if (color.x < 0) { color.x = -(color.x / 2); }
                    if (color.y < 0) { color.y = -(color.y / 2); }
                    if (color.z < 0) { color.z = -(color.z / 2); }
                    this.colors[index] = color;
                    this.uv[index] = new vec2((float)j / (float)longitudeParts, (float)i / (float)latitudeParts);

                    index++;
                }
            }

            int indexCount = (latitudeParts) * (longitudeParts)*2*3;
            this.indexes = new uint[indexCount];
            // 索引
            index = 0;
            for (int i = 0; i < latitudeParts; i++)
            {
                for (int j = 0; j < longitudeParts ; j++)
                {
                    var l = longitudeParts+1;
                    this.indexes[index + 0] = (uint)(j + i * (l));
                    this.indexes[index + 1] = (uint)(1 + j + i * (l));
                    this.indexes[index + 2] = (uint)(j + (i + 1) * (l));
                    index += 3;
                    this.indexes[index + 0] = (uint)(j + (i + 1) * (l));
                    this.indexes[index + 1] = (uint)(1 + j + i * (l));
                    this.indexes[index + 2] = (uint)(1 + j + (i + 1) * (l));
                    index += 3;
                }
                // use
                // GL.Enable(GL.GL_PRIMITIVE_RESTART);
                // GL.PrimitiveRestartIndex(uint.MaxValue);
                // GL.Disable(GL.GL_PRIMITIVE_RESTART);
                //this.indexes[index++] = uint.MaxValue;
            }
        }

        #region IBufferSource 成员

        public IEnumerable<VertexBuffer> GetVertexAttribute(string bufferName)
        {
            if (bufferName == strPosition)
            {
                if (this.positionBuffer == null)
                {
                    this.positionBuffer = positions.GenVertexBuffer(VBOConfig.Vec3, BufferUsage.StaticDraw);
                }

                yield return this.positionBuffer;
            }
            else if (bufferName == strColor)
            {
                if (this.colorBuffer == null)
                {
                    this.colorBuffer = colors.GenVertexBuffer(VBOConfig.Vec3, BufferUsage.StaticDraw);
                }

                yield return this.colorBuffer;
            }
            else if (bufferName == strUV)
            {
                if (this.uvBuffer == null)
                {
                    this.uvBuffer = uv.GenVertexBuffer(VBOConfig.Vec2, BufferUsage.StaticDraw);
                }

                yield return this.uvBuffer;
            }
            else if (bufferName == strNormal)
            {
                if (this.normalBuffer == null)
                {
                    this.normalBuffer = normals.GenVertexBuffer(VBOConfig.Vec3, BufferUsage.StaticDraw);
                }

                yield return this.normalBuffer;
            }
            else
            {
                throw new Exception(string.Format("Not expected buffer name: {0}", bufferName));
            }
        }

        public IEnumerable<IDrawCommand> GetDrawCommand()
        {
            //if (this.command == null)
            //{
            //    int vertexCount = positions.Length;
            //    this.command = new DrawArraysCmd(DrawMode.Triangles, vertexCount);
            //}
            //yield return this.command;
            if (this.command == null)
            {
                IndexBuffer buffer = indexes.GenIndexBuffer(BufferUsage.StaticDraw);
                this.command = new DrawElementsCmd(buffer, DrawMode.Triangles);
            }
            yield return this.command;

        }

        #endregion
    }
}
