﻿namespace HelgiLab
{
    partial class SphereNode
    {
        private const string regularVert = @"#version 330 core

in vec3 inPosition;
in vec2 inUV;

uniform mat4 mvpMat;

out vec2 passUV;

void main() {
    gl_Position = mvpMat * vec4(inPosition, 1.0);
    passUV = inUV;
}
";

        private const string regularFrag = @"#version 330 core

uniform sampler2D tex;

in vec2 passUV;

//layout(location = 0) out vec4 out_Color;
out vec4 outColor;

void main() {
    vec4 c0 = texture(tex, passUV);
    outColor = c0;
    //out_Color = vec4(passColor, 1.0);
}
";

    }
}
