﻿using CSharpGL;
using PanoTools.Las;
using PanoTools.Panorama;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HelgiLab.LasViewer.ViewModels
{
    public class LasViewerVM : BindableBase
    {
        string _fileName, _filePath, _dirPath, _cameraPosPath, _fotoDirPath;
        long _pointsCount;
        DateTime _beginTime, _endTime;
        CameraPosition _cameraPosition = null;
        bool _byLas, _byPano = true;
        ICommand _loadPoints;

        private Scene scene;
        private ActionList actionList;
        private Camera camera;
        private Picking pickingAction;
        //private SceneNodeBase _rootNode = null;

        public LasViewerVM(string filePath)
        {
            _filePath = filePath;
            _fileName = Path.GetFileName(_filePath);
            _dirPath = Path.GetDirectoryName(filePath);
            //_cameraPosPath = Path.Combine(_dirPath, "ladybug_panora_4mic_");
            var cameraPosPath = new Dictionary<string, bool>
            {
                { "ladybug_panora_4mic_", false },
                { "lb_panorama.txt", true },
            }
            .TakeWhile(x => !File.Exists(x.Key))
            .FirstOrDefault()
            ;
            _fotoDirPath = Path.Combine(_dirPath, "foto");
            this.Load(filePath);
        }
        public string FilePath { get => _filePath; set => SetProperty(ref _filePath, value); }
        public string FileName { get => _fileName; set => SetProperty(ref _fileName, value); }
        public long PointsCount { get => _pointsCount; set => SetProperty(ref _pointsCount, value); }
        public DateTime BeginTime { get => _beginTime; set => SetProperty(ref _beginTime, value); }
        public DateTime EndTime { get => _endTime; set => SetProperty(ref _endTime, value); }
        public bool ByLas { get => _byLas; set => SetProperty(ref _byLas, value); }
        public bool ByPano { get => _byPano; set => SetProperty(ref _byPano, value); }
        public bool HasPano => File.Exists(_cameraPosPath) && Directory.Exists(_fotoDirPath);

        public ICollection<CameraPosition> CameraPositions { get; private set; }
        public CameraPosition CameraPosition { get => _cameraPosition; set => SetProperty(ref _cameraPosition, value, CameraPositionOnChanged); }
        public bool IsIPS3 { get; private set; } = false;
        async void CameraPositionOnChanged()
        {
            if (CameraPosition == null || this.scene?.RootNode == null) return;
            SphereNode sphere = (SphereNode)this.scene.RootNode;

            await Task.Run(() => sphere.TextureSource = this.TextureSource);
        }
        public void Load(string lasFilePath)
        {
           // _lasFilePath = lasFilePath;
            _dirPath = Path.GetDirectoryName(lasFilePath);
            FileName = Path.GetFileName(lasFilePath);
            _cameraPosPath = Path.Combine(_dirPath, "ladybug_panora_4mic_");
            if (!File.Exists(_cameraPosPath))
            {
                _cameraPosPath = Path.Combine(_dirPath, "lb_panorama.txt");
                IsIPS3 = true;
            }
            _fotoDirPath = Path.Combine(_dirPath, "foto");
            using (var las = new LasEn(lasFilePath))
            {
                PointsCount = las.GetNumberOfPoints();
                BeginTime = Util.GetFromGps(las.Current.GpsTime).AddHours(3);
                las.Seek((int)PointsCount - 1);
                EndTime = Util.GetFromGps(las.Current.GpsTime).AddHours(3);

                #region получаем позиции камеры
                CameraPositions = !HasPano ? new List<CameraPosition>() : new PositionEn(_cameraPosPath, IsIPS3)
                    .ToEnumerable()
                    .ToList()
                    ;
                //CameraPosition = CameraPositions.FirstOrDefault();
                #endregion

                #region Метки времени для Las
                //LasTimes = Enumerable
                //    .Range(0, (int)(EndTime - BeginTime).TotalSeconds)
                //    .Select(i => BeginTime.AddSeconds(i))
                //    .ToList();
                #endregion
            }
            this.ByPano = true;
        }
        public ICommand LoadPointsCmd
        {
            get => _loadPoints ?? (_loadPoints = new DelegateCommand(LoadPoints, () => !string.IsNullOrWhiteSpace(FileName)).ObservesProperty(() => this.FileName));
        }
        public void LoadPoints()
        {
            
        }

        ITextureSource TextureSource
        {
            get
            {
                var dirPath = Path.GetDirectoryName(_filePath);
                var photoFileName = CameraPosition.FileName;
                var photoPosPath = Path.Combine(dirPath, photoFileName);
                var ts = new TextureSource(photoPosPath);
                return ts;
            }
        }

        public void CreateScene(WinGLCanvas winGLCanvas)
        {
            var position = new vec3(0, 0, 0);
            var direction = new vec3(0, 0, 1);
            var up = new vec3(0, 1, 0);
            var target = position + direction;

            camera = new Camera(position, target, up, CameraType.Perspecitive, winGLCanvas.Width, winGLCanvas.Height);

            this.scene = new Scene(camera)
            {
                RootNode = SphereNode.Create(),
            };

            var tansformAction = new TransformAction(scene.RootNode);
            var renderAction = new RenderAction(scene);
            var actionList = new ActionList();
            actionList.Add(tansformAction); actionList.Add(renderAction);
            this.actionList = actionList;

            this.pickingAction = new Picking(scene);

            var manipulater = new FirstPerspectiveManipulater() /*{ StepLength = 2f }*/;
            manipulater.Bind(camera, winGLCanvas);
        }
        public void OpenGLDraw()
        {
            var gl = GL.Instance;
            ActionList list = this.actionList;
            if (list != null)
            {
                vec4 clearColor = this.scene.ClearColor;
                GL.Instance.ClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
                GL.Instance.Clear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT | GL.GL_STENCIL_BUFFER_BIT);

                list.Act(new ActionParams(Viewport.GetCurrent()));
            }
        }
    }
}
