﻿using CSharpGL;
using HelgiLab.LasViewer.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HelgiLab.LasViewer.WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        LasViewerVM VM { get => DataContext as LasViewerVM; set => DataContext = value; }
        private void winGLCanvas_OpenGLDraw(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            if(this.VM != null)
                this.VM.OpenGLDraw();
        }

        private void winGLCanvas_Resize(object sender, EventArgs e)
        {
            
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        { 
            #region выбираем путь к лас файлу
            var openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "las files (*.las)|*.las|All files (*.las*)|*.las*";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() ?? false)
            {
                VM = new LasViewerVM(openFileDialog.FileName);
                WinGLCanvas winGLCanvas = (WinGLCanvas)winGLCanvasHost.Child;
                VM.CreateScene(winGLCanvas);
            }
            #endregion 
        }
    }
}
