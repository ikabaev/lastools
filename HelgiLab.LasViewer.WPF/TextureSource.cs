﻿using CSharpGL;
using System.Drawing;

namespace HelgiLab
{
    class TextureSource : ITextureSource
    {
        private readonly Texture _texture;
        public string _filename = null;
        public TextureSource(string filename)
        {
            _filename = filename;
            using (var bmp = new Bitmap(filename))
            {
                bmp.RotateFlip(RotateFlipType.Rotate180FlipY);
                var storage = new TexImageBitmap(bmp);
                _texture = new Texture(storage);
                _texture.BuiltInSampler.Add(new TexParameteri(TexParameter.PropertyName.TextureWrapS, (int)GL.GL_CLAMP_TO_EDGE));
                _texture.BuiltInSampler.Add(new TexParameteri(TexParameter.PropertyName.TextureWrapT, (int)GL.GL_CLAMP_TO_EDGE));
                _texture.BuiltInSampler.Add(new TexParameteri(TexParameter.PropertyName.TextureWrapR, (int)GL.GL_CLAMP_TO_EDGE));
                _texture.BuiltInSampler.Add(new TexParameteri(TexParameter.PropertyName.TextureMinFilter, (int)GL.GL_LINEAR));
                _texture.BuiltInSampler.Add(new TexParameteri(TexParameter.PropertyName.TextureMagFilter, (int)GL.GL_LINEAR));

                _texture.Initialize();
            }
        }

        #region ITextureSource 成员

        public Texture BindingTexture { get { return _texture; } }

        #endregion
    }
}
