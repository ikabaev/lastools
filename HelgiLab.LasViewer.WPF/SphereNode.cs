﻿using CSharpGL;

namespace HelgiLab
{
    partial class SphereNode : PickableNode, IRenderable
    {
        private PolygonModeSwitch polygonMode = new PolygonModeSwitch(PolygonMode.Line);

        private const string tex = "tex";
        private const string inUV = "inUV";
        public static SphereNode Create()
        {
            var model = new SphereModel(1000, 51, 51);
            var vs = new VertexShader(regularVert);
            var fs = new FragmentShader(regularFrag);
            var array = new ShaderArray(vs, fs);

            var map = new AttributeMap();
            map.Add("inPosition", SphereModel.strPosition);
            //map.Add("inColor", SphereModel.strColor);
            map.Add(inUV, SphereModel.strUV);
            var builder = new RenderMethodBuilder(array, map);
            var node = new SphereNode(model, SphereModel.strPosition, builder);
            
            node.Initialize();

            return node;
        }
        public bool Show = true;
        private SphereNode(IBufferSource model, string positionNameInIBufferSource, params RenderMethodBuilder[] builders)
            : base(model, positionNameInIBufferSource, builders)
        {
        }
        //private ITextureSource GetTexture()
        //{
        //    //string folder = System.Windows.Forms.Application.StartupPath;
        //    //var bmp = new Bitmap(@"D:\1187-pp001-2018-06-29-08-24-34\foto\posed_lb_pics000001.jpg");
        //    //TexStorageBase storage = new TexImageBitmap(bmp);
        //    //var texture = new Texture(storage,
        //    //    new TexParameteri(TexParameter.PropertyName.TextureWrapS, (int)GL.GL_CLAMP_TO_EDGE),
        //    //    new TexParameteri(TexParameter.PropertyName.TextureWrapT, (int)GL.GL_CLAMP_TO_EDGE),
        //    //    new TexParameteri(TexParameter.PropertyName.TextureWrapR, (int)GL.GL_CLAMP_TO_EDGE),
        //    //    new TexParameteri(TexParameter.PropertyName.TextureMinFilter, (int)GL.GL_LINEAR),
        //    //    new TexParameteri(TexParameter.PropertyName.TextureMagFilter, (int)GL.GL_LINEAR));
        //    //texture.Initialize();
        //    //bmp.Dispose();

        //    //return texture;
        //    return new TextureSource(@"D:\1187-pp001-2018-06-29-08-24-34\foto\posed_lb_pics000001.jpg");
        //}
        public ITextureSource TextureSource { get; set; }
        #region IRenderable 成员

        public ThreeFlags EnableRendering { get { return ThreeFlags.BeforeChildren | ThreeFlags.Children; } set { } }
        //public ThreeFlags EnableRendering { get; set; } = ThreeFlags.BeforeChildren;

        TextureSource _tex;
        public void RenderBeforeChildren(RenderEventArgs arg)
        {
            ICamera camera = arg.Camera;
            mat4 projection = camera.GetProjectionMatrix();
            mat4 view = camera.GetViewMatrix();
            mat4 model = this.GetModelMatrix();

            RenderMethod method = this.RenderUnit.Methods[0];
            ShaderProgram program = method.Program;
            program.SetUniform("mvpMat", projection * view * model);

            //if (TextureSource == null)
            //    TextureSource = GetTexture();
            //var _tex = (TextureSource)TextureSource; 
            //if (_tex != null)
            //    program.SetUniform(tex, new TextureSource(_tex._filename).BindingTexture);

            // не пойму почему program.SetUniform(tex, TextureSource.BindingTexture) не работает;
            if (Show && TextureSource != null && _tex?._filename != (this.TextureSource as TextureSource)?._filename)
            {
                _tex = new TextureSource(((TextureSource)TextureSource)._filename);
            }
            //else if (TextureSource == null)
            //{
            //    _tex = null;
            //}
            if (_tex != null)
                program.SetUniform(tex, _tex?.BindingTexture);
            

            //this.polygonMode.On();
            if (Show)
                method.Render();
            //this.polygonMode.Off();
        }

        public void RenderAfterChildren(RenderEventArgs arg)
        {
        }

        #endregion

    }
}
