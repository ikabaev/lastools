﻿namespace HelgiLab
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPagePoints = new System.Windows.Forms.TabPage();
            this.winGLCanvasPoints = new CSharpGL.WinGLCanvas();
            this.tabPagePhoto = new System.Windows.Forms.TabPage();
            this.panelPhoto = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxPointsShow = new System.Windows.Forms.CheckBox();
            this.lblColor1 = new System.Windows.Forms.Label();
            this.lblColor2 = new System.Windows.Forms.Label();
            this.lblColor3 = new System.Windows.Forms.Label();
            this.lblColor4 = new System.Windows.Forms.Label();
            this.comboBoxPointsColor = new System.Windows.Forms.ComboBox();
            this.lblColor5 = new System.Windows.Forms.Label();
            this.comboBoxPointSize = new System.Windows.Forms.ComboBox();
            this.chB1 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.chB2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panelShowPano = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonRotationApply = new System.Windows.Forms.Button();
            this.checkBoxPanoShow = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxRotateZ = new System.Windows.Forms.TextBox();
            this.textBoxRotateX = new System.Windows.Forms.TextBox();
            this.textBoxRotateY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonBackToCameraPosition = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonLas = new System.Windows.Forms.RadioButton();
            this.radioButtonPano = new System.Windows.Forms.RadioButton();
            this.comboBoxLasTimes = new System.Windows.Forms.ComboBox();
            this.labelfileName = new System.Windows.Forms.Label();
            this.labelBeginTime = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.Label();
            this.panelViewLas = new System.Windows.Forms.Panel();
            this.progressBarReadLassPB = new System.Windows.Forms.ProgressBar();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSeek = new System.Windows.Forms.Button();
            this.comboBoxPointsCount = new System.Windows.Forms.ComboBox();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPagePoints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.winGLCanvasPoints)).BeginInit();
            this.tabPagePhoto.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelShowPano.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelViewLas.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.printToolStripButton,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1011, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.Visible = false;
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Visible = false;
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "&Print";
            this.printToolStripButton.Visible = false;
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripButton.Image")));
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "C&ut";
            this.cutToolStripButton.Visible = false;
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "&Copy";
            this.copyToolStripButton.Visible = false;
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "&Paste";
            this.pasteToolStripButton.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "He&lp";
            this.helpToolStripButton.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPagePoints);
            this.tabControl1.Controls.Add(this.tabPagePhoto);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1011, 649);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPagePoints
            // 
            this.tabPagePoints.Controls.Add(this.winGLCanvasPoints);
            this.tabPagePoints.Location = new System.Drawing.Point(4, 22);
            this.tabPagePoints.Name = "tabPagePoints";
            this.tabPagePoints.Size = new System.Drawing.Size(1003, 623);
            this.tabPagePoints.TabIndex = 0;
            this.tabPagePoints.Text = "Облако точек";
            this.tabPagePoints.UseVisualStyleBackColor = true;
            // 
            // winGLCanvasPoints
            // 
            this.winGLCanvasPoints.AccumAlphaBits = ((byte)(0));
            this.winGLCanvasPoints.AccumBits = ((byte)(0));
            this.winGLCanvasPoints.AccumBlueBits = ((byte)(0));
            this.winGLCanvasPoints.AccumGreenBits = ((byte)(0));
            this.winGLCanvasPoints.AccumRedBits = ((byte)(0));
            this.winGLCanvasPoints.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.winGLCanvasPoints.Dock = System.Windows.Forms.DockStyle.Fill;
            this.winGLCanvasPoints.Location = new System.Drawing.Point(0, 0);
            this.winGLCanvasPoints.Name = "winGLCanvasPoints";
            this.winGLCanvasPoints.RenderTrigger = CSharpGL.RenderTrigger.TimerBased;
            this.winGLCanvasPoints.Size = new System.Drawing.Size(1003, 623);
            this.winGLCanvasPoints.StencilBits = ((byte)(0));
            this.winGLCanvasPoints.TabIndex = 7;
            this.winGLCanvasPoints.TimerTriggerInterval = 40;
            this.winGLCanvasPoints.UpdateContextVersion = true;
            this.winGLCanvasPoints.OpenGLDraw += new CSharpGL.GLEventHandler<System.Windows.Forms.PaintEventArgs>(this.winGLCanvasPoints_OpenGLDraw);
            // 
            // tabPagePhoto
            // 
            this.tabPagePhoto.Controls.Add(this.panelPhoto);
            this.tabPagePhoto.Location = new System.Drawing.Point(4, 22);
            this.tabPagePhoto.Name = "tabPagePhoto";
            this.tabPagePhoto.Size = new System.Drawing.Size(1003, 623);
            this.tabPagePhoto.TabIndex = 1;
            this.tabPagePhoto.Text = "Панорама";
            this.tabPagePhoto.UseVisualStyleBackColor = true;
            // 
            // panelPhoto
            // 
            this.panelPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPhoto.Location = new System.Drawing.Point(0, 0);
            this.panelPhoto.Name = "panelPhoto";
            this.panelPhoto.Size = new System.Drawing.Size(1003, 623);
            this.panelPhoto.TabIndex = 0;
            this.panelPhoto.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPhoto_Paint);
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.groupBox2);
            this.panelControls.Controls.Add(this.panelShowPano);
            this.panelControls.Controls.Add(this.groupBox3);
            this.panelControls.Controls.Add(this.groupBox1);
            this.panelControls.Controls.Add(this.labelfileName);
            this.panelControls.Controls.Add(this.labelBeginTime);
            this.panelControls.Controls.Add(this.labelCount);
            this.panelControls.Controls.Add(this.panelViewLas);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControls.Enabled = false;
            this.panelControls.Location = new System.Drawing.Point(790, 25);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(221, 649);
            this.panelControls.TabIndex = 8;
            this.panelControls.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.checkBoxPointsShow);
            this.groupBox2.Controls.Add(this.lblColor1);
            this.groupBox2.Controls.Add(this.lblColor2);
            this.groupBox2.Controls.Add(this.lblColor3);
            this.groupBox2.Controls.Add(this.lblColor4);
            this.groupBox2.Controls.Add(this.comboBoxPointsColor);
            this.groupBox2.Controls.Add(this.lblColor5);
            this.groupBox2.Controls.Add(this.comboBoxPointSize);
            this.groupBox2.Controls.Add(this.chB1);
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.chB2);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Location = new System.Drawing.Point(3, 284);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 145);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(109, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Цвет точек";
            // 
            // checkBoxPointsShow
            // 
            this.checkBoxPointsShow.AutoSize = true;
            this.checkBoxPointsShow.Checked = true;
            this.checkBoxPointsShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPointsShow.Location = new System.Drawing.Point(8, 12);
            this.checkBoxPointsShow.Name = "checkBoxPointsShow";
            this.checkBoxPointsShow.Size = new System.Drawing.Size(119, 17);
            this.checkBoxPointsShow.TabIndex = 4;
            this.checkBoxPointsShow.Text = "Отображать точки";
            this.checkBoxPointsShow.UseVisualStyleBackColor = true;
            this.checkBoxPointsShow.CheckedChanged += new System.EventHandler(this.checkBoxPanoShow_CheckedChanged);
            // 
            // lblColor1
            // 
            this.lblColor1.BackColor = System.Drawing.Color.MediumBlue;
            this.lblColor1.Font = new System.Drawing.Font("SimSun", 12F);
            this.lblColor1.Location = new System.Drawing.Point(52, 35);
            this.lblColor1.Name = "lblColor1";
            this.lblColor1.Size = new System.Drawing.Size(43, 20);
            this.lblColor1.TabIndex = 3;
            this.lblColor1.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor2
            // 
            this.lblColor2.BackColor = System.Drawing.Color.Orange;
            this.lblColor2.Font = new System.Drawing.Font("SimSun", 12F);
            this.lblColor2.Location = new System.Drawing.Point(52, 55);
            this.lblColor2.Name = "lblColor2";
            this.lblColor2.Size = new System.Drawing.Size(43, 20);
            this.lblColor2.TabIndex = 3;
            this.lblColor2.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor3
            // 
            this.lblColor3.BackColor = System.Drawing.Color.YellowGreen;
            this.lblColor3.Font = new System.Drawing.Font("SimSun", 12F);
            this.lblColor3.Location = new System.Drawing.Point(52, 75);
            this.lblColor3.Name = "lblColor3";
            this.lblColor3.Size = new System.Drawing.Size(43, 20);
            this.lblColor3.TabIndex = 3;
            this.lblColor3.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // lblColor4
            // 
            this.lblColor4.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.lblColor4.Font = new System.Drawing.Font("SimSun", 12F);
            this.lblColor4.Location = new System.Drawing.Point(52, 95);
            this.lblColor4.Name = "lblColor4";
            this.lblColor4.Size = new System.Drawing.Size(43, 20);
            this.lblColor4.TabIndex = 3;
            this.lblColor4.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // comboBoxPointsColor
            // 
            this.comboBoxPointsColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointsColor.FormattingEnabled = true;
            this.comboBoxPointsColor.Location = new System.Drawing.Point(112, 54);
            this.comboBoxPointsColor.Name = "comboBoxPointsColor";
            this.comboBoxPointsColor.Size = new System.Drawing.Size(84, 21);
            this.comboBoxPointsColor.TabIndex = 7;
            // 
            // lblColor5
            // 
            this.lblColor5.BackColor = System.Drawing.Color.DarkCyan;
            this.lblColor5.Font = new System.Drawing.Font("SimSun", 12F);
            this.lblColor5.Location = new System.Drawing.Point(52, 115);
            this.lblColor5.Name = "lblColor5";
            this.lblColor5.Size = new System.Drawing.Size(43, 20);
            this.lblColor5.TabIndex = 3;
            this.lblColor5.Click += new System.EventHandler(this.lblColor_Click);
            // 
            // comboBoxPointSize
            // 
            this.comboBoxPointSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointSize.FormattingEnabled = true;
            this.comboBoxPointSize.Location = new System.Drawing.Point(113, 101);
            this.comboBoxPointSize.Name = "comboBoxPointSize";
            this.comboBoxPointSize.Size = new System.Drawing.Size(84, 21);
            this.comboBoxPointSize.TabIndex = 7;
            // 
            // chB1
            // 
            this.chB1.AutoSize = true;
            this.chB1.Checked = true;
            this.chB1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chB1.Location = new System.Drawing.Point(8, 38);
            this.chB1.Name = "chB1";
            this.chB1.Size = new System.Drawing.Size(42, 17);
            this.chB1.TabIndex = 4;
            this.chB1.Text = "ПП";
            this.chB1.UseVisualStyleBackColor = true;
            this.chB1.CheckedChanged += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Checked = true;
            this.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox5.Location = new System.Drawing.Point(8, 118);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(42, 17);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "ПЛ";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // chB2
            // 
            this.chB2.AutoSize = true;
            this.chB2.Checked = true;
            this.chB2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chB2.Location = new System.Drawing.Point(8, 58);
            this.chB2.Name = "chB2";
            this.chB2.Size = new System.Drawing.Size(41, 17);
            this.chB2.TabIndex = 4;
            this.chB2.Text = "ЗП";
            this.chB2.UseVisualStyleBackColor = true;
            this.chB2.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Location = new System.Drawing.Point(8, 98);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(41, 17);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Text = "ЗЛ";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Размер точек";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(8, 78);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(33, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "З";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Click += new System.EventHandler(this.chB_CheckedChanged);
            // 
            // panelShowPano
            // 
            this.panelShowPano.Controls.Add(this.label4);
            this.panelShowPano.Controls.Add(this.buttonRotationApply);
            this.panelShowPano.Controls.Add(this.checkBoxPanoShow);
            this.panelShowPano.Controls.Add(this.label6);
            this.panelShowPano.Controls.Add(this.textBoxRotateZ);
            this.panelShowPano.Controls.Add(this.textBoxRotateX);
            this.panelShowPano.Controls.Add(this.textBoxRotateY);
            this.panelShowPano.Controls.Add(this.label5);
            this.panelShowPano.Location = new System.Drawing.Point(3, 210);
            this.panelShowPano.Name = "panelShowPano";
            this.panelShowPano.Size = new System.Drawing.Size(210, 72);
            this.panelShowPano.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Углы: X";
            // 
            // buttonRotationApply
            // 
            this.buttonRotationApply.Location = new System.Drawing.Point(7, 46);
            this.buttonRotationApply.Name = "buttonRotationApply";
            this.buttonRotationApply.Size = new System.Drawing.Size(198, 23);
            this.buttonRotationApply.TabIndex = 2;
            this.buttonRotationApply.Text = "Довернуть";
            this.buttonRotationApply.UseVisualStyleBackColor = true;
            this.buttonRotationApply.Click += new System.EventHandler(this.buttonRotationApply_Click);
            // 
            // checkBoxPanoShow
            // 
            this.checkBoxPanoShow.AutoSize = true;
            this.checkBoxPanoShow.Checked = true;
            this.checkBoxPanoShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPanoShow.Location = new System.Drawing.Point(8, 3);
            this.checkBoxPanoShow.Name = "checkBoxPanoShow";
            this.checkBoxPanoShow.Size = new System.Drawing.Size(140, 17);
            this.checkBoxPanoShow.TabIndex = 4;
            this.checkBoxPanoShow.Text = "Отображать панораму";
            this.checkBoxPanoShow.UseVisualStyleBackColor = true;
            this.checkBoxPanoShow.CheckedChanged += new System.EventHandler(this.checkBoxPanoShow_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Z";
            // 
            // textBoxRotateZ
            // 
            this.textBoxRotateZ.Location = new System.Drawing.Point(162, 20);
            this.textBoxRotateZ.Name = "textBoxRotateZ";
            this.textBoxRotateZ.Size = new System.Drawing.Size(42, 20);
            this.textBoxRotateZ.TabIndex = 8;
            // 
            // textBoxRotateX
            // 
            this.textBoxRotateX.AllowDrop = true;
            this.textBoxRotateX.Location = new System.Drawing.Point(51, 20);
            this.textBoxRotateX.Name = "textBoxRotateX";
            this.textBoxRotateX.Size = new System.Drawing.Size(42, 20);
            this.textBoxRotateX.TabIndex = 8;
            // 
            // textBoxRotateY
            // 
            this.textBoxRotateY.Location = new System.Drawing.Point(105, 20);
            this.textBoxRotateY.Name = "textBoxRotateY";
            this.textBoxRotateY.Size = new System.Drawing.Size(42, 20);
            this.textBoxRotateY.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Y";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonBackToCameraPosition);
            this.groupBox3.Location = new System.Drawing.Point(3, 435);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(210, 53);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сместить точку просмотра";
            // 
            // buttonBackToCameraPosition
            // 
            this.buttonBackToCameraPosition.Location = new System.Drawing.Point(3, 19);
            this.buttonBackToCameraPosition.Name = "buttonBackToCameraPosition";
            this.buttonBackToCameraPosition.Size = new System.Drawing.Size(208, 24);
            this.buttonBackToCameraPosition.TabIndex = 2;
            this.buttonBackToCameraPosition.Text = "В исходную точку";
            this.buttonBackToCameraPosition.UseVisualStyleBackColor = true;
            this.buttonBackToCameraPosition.Click += new System.EventHandler(this.buttonBackToCameraPosition_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonLas);
            this.groupBox1.Controls.Add(this.radioButtonPano);
            this.groupBox1.Controls.Add(this.comboBoxLasTimes);
            this.groupBox1.Location = new System.Drawing.Point(6, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(205, 70);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Область просмотра";
            // 
            // radioButtonLas
            // 
            this.radioButtonLas.AutoSize = true;
            this.radioButtonLas.Location = new System.Drawing.Point(100, 19);
            this.radioButtonLas.Name = "radioButtonLas";
            this.radioButtonLas.Size = new System.Drawing.Size(96, 17);
            this.radioButtonLas.TabIndex = 0;
            this.radioButtonLas.Text = "По LAS-файлу";
            this.radioButtonLas.UseVisualStyleBackColor = true;
            this.radioButtonLas.CheckedChanged += new System.EventHandler(this.radioButtonViewArea_CheckedChanged);
            // 
            // radioButtonPano
            // 
            this.radioButtonPano.AutoSize = true;
            this.radioButtonPano.Checked = true;
            this.radioButtonPano.Location = new System.Drawing.Point(6, 19);
            this.radioButtonPano.Name = "radioButtonPano";
            this.radioButtonPano.Size = new System.Drawing.Size(92, 17);
            this.radioButtonPano.TabIndex = 0;
            this.radioButtonPano.TabStop = true;
            this.radioButtonPano.Text = "По панораме";
            this.radioButtonPano.UseVisualStyleBackColor = true;
            this.radioButtonPano.CheckedChanged += new System.EventHandler(this.radioButtonViewArea_CheckedChanged);
            // 
            // comboBoxLasTimes
            // 
            this.comboBoxLasTimes.DisplayMember = "\"yyyy MM dd HH mm ss\"";
            this.comboBoxLasTimes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLasTimes.FormattingEnabled = true;
            this.comboBoxLasTimes.Location = new System.Drawing.Point(7, 42);
            this.comboBoxLasTimes.Name = "comboBoxLasTimes";
            this.comboBoxLasTimes.Size = new System.Drawing.Size(192, 21);
            this.comboBoxLasTimes.TabIndex = 5;
            // 
            // labelfileName
            // 
            this.labelfileName.AutoSize = true;
            this.labelfileName.Location = new System.Drawing.Point(9, 9);
            this.labelfileName.Name = "labelfileName";
            this.labelfileName.Size = new System.Drawing.Size(84, 13);
            this.labelfileName.TabIndex = 1;
            this.labelfileName.Tag = "Текущий файл: {0}";
            this.labelfileName.Text = "Текущий файл:";
            // 
            // labelBeginTime
            // 
            this.labelBeginTime.AutoSize = true;
            this.labelBeginTime.Location = new System.Drawing.Point(9, 43);
            this.labelBeginTime.Name = "labelBeginTime";
            this.labelBeginTime.Size = new System.Drawing.Size(50, 13);
            this.labelBeginTime.TabIndex = 1;
            this.labelBeginTime.Tag = "Начало: {0}";
            this.labelBeginTime.Text = "Начало: ";
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Location = new System.Drawing.Point(9, 26);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(45, 13);
            this.labelCount.TabIndex = 1;
            this.labelCount.Tag = "Объем: {0}";
            this.labelCount.Text = "Объем:";
            // 
            // panelViewLas
            // 
            this.panelViewLas.Controls.Add(this.progressBarReadLassPB);
            this.panelViewLas.Controls.Add(this.label7);
            this.panelViewLas.Controls.Add(this.buttonSeek);
            this.panelViewLas.Controls.Add(this.comboBoxPointsCount);
            this.panelViewLas.Location = new System.Drawing.Point(3, 131);
            this.panelViewLas.Name = "panelViewLas";
            this.panelViewLas.Size = new System.Drawing.Size(210, 75);
            this.panelViewLas.TabIndex = 12;
            // 
            // progressBarReadLassPB
            // 
            this.progressBarReadLassPB.Location = new System.Drawing.Point(8, 60);
            this.progressBarReadLassPB.Name = "progressBarReadLassPB";
            this.progressBarReadLassPB.Size = new System.Drawing.Size(194, 10);
            this.progressBarReadLassPB.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 1;
            this.label7.Tag = "";
            this.label7.Text = "Число точек:";
            // 
            // buttonSeek
            // 
            this.buttonSeek.Location = new System.Drawing.Point(7, 36);
            this.buttonSeek.Name = "buttonSeek";
            this.buttonSeek.Size = new System.Drawing.Size(196, 23);
            this.buttonSeek.TabIndex = 2;
            this.buttonSeek.Text = "Загрузить точки";
            this.buttonSeek.UseVisualStyleBackColor = true;
            this.buttonSeek.Click += new System.EventHandler(this.buttonSeek_Click);
            // 
            // comboBoxPointsCount
            // 
            this.comboBoxPointsCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPointsCount.FormattingEnabled = true;
            this.comboBoxPointsCount.Location = new System.Drawing.Point(86, 7);
            this.comboBoxPointsCount.Name = "comboBoxPointsCount";
            this.comboBoxPointsCount.Size = new System.Drawing.Size(117, 21);
            this.comboBoxPointsCount.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 674);
            this.Controls.Add(this.panelControls);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "HelgiLab Las Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPagePoints.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.winGLCanvasPoints)).EndInit();
            this.tabPagePhoto.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelShowPano.ResumeLayout(false);
            this.panelShowPano.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelViewLas.ResumeLayout(false);
            this.panelViewLas.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPagePoints;
        private System.Windows.Forms.Button buttonSeek;
        private CSharpGL.WinGLCanvas winGLCanvasPoints;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox chB2;
        private System.Windows.Forms.CheckBox chB1;
        private System.Windows.Forms.Label lblColor5;
        private System.Windows.Forms.Label lblColor4;
        private System.Windows.Forms.Label lblColor3;
        private System.Windows.Forms.Label lblColor2;
        private System.Windows.Forms.Label lblColor1;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.ProgressBar progressBarReadLassPB;
        private System.Windows.Forms.ComboBox comboBoxLasTimes;
        private System.Windows.Forms.TabPage tabPagePhoto;
        private System.Windows.Forms.Panel panelPhoto;
        private System.Windows.Forms.ComboBox comboBoxPointSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRotateZ;
        private System.Windows.Forms.TextBox textBoxRotateY;
        private System.Windows.Forms.TextBox textBoxRotateX;
        private System.Windows.Forms.Button buttonRotationApply;
        private System.Windows.Forms.CheckBox checkBoxPanoShow;
        private System.Windows.Forms.Button buttonBackToCameraPosition;
        private System.Windows.Forms.ComboBox comboBoxPointsColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelfileName;
        private System.Windows.Forms.Label labelBeginTime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonLas;
        private System.Windows.Forms.RadioButton radioButtonPano;
        private System.Windows.Forms.ComboBox comboBoxPointsCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxPointsShow;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panelViewLas;
        private System.Windows.Forms.Panel panelShowPano;
    }
}

