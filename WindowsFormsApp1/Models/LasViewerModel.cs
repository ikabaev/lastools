﻿using PanoTools.Las;
using PanoTools.Panorama;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelgiLab.Models
{
    public enum PositionType { Pano, Las }
    public class LasViewerModel
    {
        string _dirPath, _cameraPosPath, _fotoDirPath, _lasFilePath;

        public string FileName { get; private set; }
        public long PointsCount { get; private set; }
        public DateTime BeginTime { get; private set; }
        public DateTime EndTime { get; private set; }
        public PositionType PositionType { get; set; } = PositionType.Las;
        public ICollection<CameraPosition> CameraPositions { get; private set; }
        public ICollection<DateTime> LasTimes { get; private set; }
        public static IList<KeyValuePair<int, string>> PointsCounts { get; private set; } = new Dictionary<int, string>
        {
            { 100000, "100 тыс." }, { 300000, "300 тыс." }, { 1000000, "1 млн." }, { 2000000, "2 млн." }, { 5000000, "5 млн." }, { 10000000, "10 млн." }, { 20000000, "20 млн." }
        }
        .ToList();

        public static int GetPointsCount(int index) => PointsCounts[index].Key;
        public bool HasPano => File.Exists(_cameraPosPath) && Directory.Exists(_fotoDirPath);
        public LasViewerModel() { }
        public LasViewerModel(string lasFilePath)
        {
            Load(lasFilePath);
        }
        public void Load(string lasFilePath)
        {
            _lasFilePath = lasFilePath;
            _dirPath = Path.GetDirectoryName(lasFilePath);
            FileName = Path.GetFileName(lasFilePath);
            _cameraPosPath = Path.Combine(_dirPath, "ladybug_panora_4mic_");
            _fotoDirPath = Path.Combine(_dirPath, "foto");

            using (var las = new LasEn(lasFilePath))
            {
                PointsCount = las.GetNumberOfPoints();
                BeginTime = Util.GetFromGps(las.Current.GpsTime).AddHours(3);
                las.Seek((int)PointsCount - 1);
                EndTime = Util.GetFromGps(las.Current.GpsTime).AddHours(3);

                #region получаем позиции камеры
                CameraPositions = !HasPano ? new List<CameraPosition>() : new PositionEn(_cameraPosPath)
                    .ToEnumerable()
                    // вычисляем
                    .Select(_ =>
                    {
                        las.ConvertFields(_.X, _.Y, _.Z, out _.X, out _.Y, out _.Z);
                        return _;

                    })
                    .ToList()
                    ;
                #endregion

                #region Метки времени для Las
                LasTimes = Enumerable
                    .Range(0, (int)(EndTime - BeginTime).TotalSeconds)
                    .Select(i => BeginTime.AddSeconds(i))
                    .ToList();
                #endregion
            }
        }

        public int? FindPosByTime(DateTime seekTime)
        {
            int? seek = null;
            using (var las = new LasEn(_lasFilePath))
            {
                int l = 0, r = (int)las.GetNumberOfPoints() - 1;

                double dSec = 2;
                int curPos = 5;
                do
                {
                    curPos = (l + r) / 2;

                    las.Seek(curPos);
                    var t = Util.GetFromGps(las.Current.GpsTime);

                    //dPos /= 2;

                    dSec = (seekTime - t).TotalSeconds;
                    if (dSec > 0)
                    {
                        l = curPos;
                    }
                    else
                    {
                        r = curPos;
                    }
                }
                while ((r-l) > 2 && Math.Abs(dSec) > 1);
                seek = curPos;
            }
            return seek;
        }
    }
}
