﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelgiLab
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                var form = new MainForm { Text = $"HelgiLab Las Viewer  - V {Application.ProductVersion}"};
                Application.Run(form);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //IBank<DepositAccount> depositBank = new Bank();
            //depositBank.DoOperation();

            //IBank<Account> ordinaryBank = depositBank;
            //ordinaryBank.DoOperation();

            //Account account = new Account();
            //IBank<Account> ordinaryBank = new Bank<Account>();
            ////ordinaryBank.DoOperation(account);

            //DepositAccount depositAcc = new DepositAccount();
            //IBank<DepositAccount> depositBank = ordinaryBank;
            //depositBank.DoOperation(depositAcc);

        }
    }
}
