﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSharpGL;
using PanoTools.Panorama;

namespace HelgiLab
{
    partial class PointsNode : ModernNode, IRenderable
    {
        public enum EMethod { Random, gl_VertexID, };
        public EMethod Method { get; set; }
        public vec3 Color { get; set; }
        public bool ShowRealColors { get; set; } = true;
        public int PointSize { get; set; } = 1;
        CameraPosition _cameraPosition = null;
        mat4 _rt;
        public CameraPosition CameraPosition
        {
            get => _cameraPosition;
            set
            {
                _cameraPosition = value;
                //Quaternion = PanoTools.Quaternion.CreateFromYawPitchRoll(
                //    -_cameraPosition.AttitudeY * Math.PI / 180,
                //    _cameraPosition.AttitudeZ * Math.PI / 180 + Math.PI,
                //    Math.PI - _cameraPosition.AttitudeX * Math.PI / 180);

                //var m = Quaternion.ToMatrix();
                //vec4 col0 = new vec4((float)m.M11, (float)m.M21, (float)m.M31, (float)m.M41);
                //vec4 col1 = new vec4((float)m.M12, (float)m.M22, (float)m.M32, (float)m.M42);
                //vec4 col2 = new vec4((float)m.M13, (float)m.M23, (float)m.M33, (float)m.M43);
                //vec4 col3 = new vec4((float)m.M14, (float)m.M24, (float)m.M34, (float)m.M44);

                //_rt = new mat4(col0, col1, col2, col3);

                //var rt = glm.rotate((float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));
                //rt = glm.rotate(rt, (float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                //rt = glm.rotate(rt, (float)(-CameraPosition.AttitudeY), new vec3(1, 0, 0));


                
                var rt = glm.rotate((float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                rt = glm.rotate(rt, (float)(CameraPosition.AttitudeY), new vec3(1, 0, 0));
                rt = glm.rotate(rt, (float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));

                _rt = rt;
            }
        }
        PanoTools.Quaternion Quaternion { get; set; }
        //public vec4? Quaternion { get; set; }
        public vec3? Shift { get; set; }
        public static PointsNode Create(IBufferSource model, string position, string color/*, vec3 size*/)
        {
            RenderMethodBuilder randomBuilder/*, gl_VertexIDBuilder*/;
            var pointSizeSwitch = new PointSizeSwitch(5);
            {
                var vs = new VertexShader(randomVert);
                var fs = new FragmentShader(randomFrag);
                var array = new ShaderArray(vs, fs);
                var map = new AttributeMap();
                map.Add("inPosition", position);
                map.Add("inColor", color);
                randomBuilder = new RenderMethodBuilder(array, map, pointSizeSwitch);
            }
            //{
            //    var vs = new VertexShader(gl_VertexIDVert);
            //    var fs = new FragmentShader(gl_VertexIDFrag);
            //    var array = new ShaderArray(vs, fs);
            //    var map = new AttributeMap();
            //    map.Add("inPosition", position);
            //    gl_VertexIDBuilder = new RenderMethodBuilder(array, map, pointSizeSwitch);
            //}

            var node = new PointsNode(model, randomBuilder/*, gl_VertexIDBuilder*/);
            node.Initialize();
            //node.ModelSize = size;

            return node;
        }

        private PointsNode(IBufferSource model, params RenderMethodBuilder[] builders)
            : base(model, builders)
        {
            this.EnableRendering = ThreeFlags.BeforeChildren | ThreeFlags.Children;
        }

        #region IRenderable 成员

        public ThreeFlags EnableRendering { get; set; }

        public void RenderBeforeChildren(RenderEventArgs arg)
        {
            ICamera camera = arg.Camera;
            mat4 projection = camera.GetProjectionMatrix();
            mat4 view = camera.GetViewMatrix();
            mat4 model = this.GetModelMatrix();
            
            var method = this.RenderUnit.Methods[(int)this.Method];
            ShaderProgram program = method.Program;
            if (CameraPosition != null)
            {
                //var angle = Math.Acos(0.99998496531458f) * 2 * 180;
                //if (Shift != null)
                //    view = glm.translate(view, Shift.Value);
                //var rt = glm.rotate(0.99998496531458f, new vec3(-0.000456684493859697f, -0.00527136408327128f, 0.00143989748679045f));
                //var rt = glm.rotate((float)Quaternion.W, new vec3((float)Quaternion.X, (float)Quaternion.Y, (float)Quaternion.Z));

                //var ax = CameraPosition.AttitudeX < 0 ? CameraPosition.AttitudeX + 180 : CameraPosition.AttitudeX - 180;
                //var rt = glm.rotate((float)(CameraPosition.AttitudeX > 0 ? 180 + CameraPosition.AttitudeX : 180 - CameraPosition.AttitudeX), new vec3(1, 0, 0));

                //var rt = glm.rotate((float)(180 + CameraPosition.AttitudeX), new vec3(1, 0, 0));
                //rt = glm.rotate(rt, (float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));
                //rt = glm.rotate(rt, (float)CameraPosition.AttitudeY, new vec3(0, 0, 1));



                //var rt = glm.rotate((float)CameraPosition.AttitudeZ + 180, new vec3(0, 1, 0));
                //rt = glm.rotate(rt, (float)(180 - CameraPosition.AttitudeX), new vec3(0, 0, 1));
                //rt = glm.rotate(rt, (float)(-CameraPosition.AttitudeY), new vec3(1, 0, 0));

                //if (Quaternion != null)
                //    rt = glm.rotate(rt, (float)angle, new vec3(Quaternion.Value[0], Quaternion.Value[1], Quaternion.Value[2]));

                //view = view * rt; 
                //rt = glm.rotate(rt, (float)angle, new vec3(-0.000456684493859697f, -0.00527136408327128f, 0.00143989748679045f));

                program.SetUniform("mvpMatrix", projection * view * _rt * model );
            }
            else
                program.SetUniform("mvpMatrix", projection * view * model);
            
            program.SetUniform("enableRendering", EnableRendering != ThreeFlags.None);
            program.SetUniform("oneColor", this.Color);
            program.SetUniform("realColor", this.ShowRealColors);
            program.SetUniform("pointSize", this.PointSize);
                
            GL.Instance.Enable(GL.GL_VERTEX_PROGRAM_POINT_SIZE);
            method.Render();
        }

        public void RenderAfterChildren(RenderEventArgs arg)
        {
        }

        #endregion
    }
}
