﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanoTools
{
    public class PointLatLonConverter
    {
        double center_lat, center_lon, scaley, scalex, center_z;
        public PointLatLonConverter(double firstLon, double firstLat, double firstZ)
        {
            center_lat = firstLat;
            center_lon = firstLon;
            center_z = firstZ;

            scaley = GetDistance(center_lat, center_lon, center_lat + 0.001, center_lon) * 1000; // to gradus
            scalex = GetDistance(center_lat, center_lon, center_lat, center_lon + 0.001) * 1000;
        }
        public double GetDistance(double lat1, double lon1, double lat2, double lon2)
        {
            const double M_PI = Math.PI;
            Func<double, double> sin = (_ => Math.Sin(_));
            Func<double, double> cos = (_ => Math.Cos(_));
            Func<double, double> sqrt = (_ => Math.Sqrt(_));
            Func<double, double, double> atan2 = ((x,y) => Math.Atan2(x, y));

            const double R = 6371000; // m
            lat1 = lat1 * M_PI / 180;
            lon1 = lon1 * M_PI / 180;
            lat2 = lat2 * M_PI / 180;
            lon2 = lon2 * M_PI / 180;
            double dLat = ((double)lat2 - lat1);
            double dLon = ((double)lon2 - lon1);

            double a = sin(dLat / 2) * sin(dLat / 2) +
                sin(dLon / 2) * sin(dLon / 2) * cos(lat1) * cos((double)lat2);
            double c = 2 * atan2(sqrt(a), sqrt(1 - a));
            double d = R * c;

            return d;
        }
        public double GetDistance2(double lat1, double lon1, double lat2, double lon2)
        {
            const double M_PI = Math.PI;
            Func<double, double> sin = (_ => Math.Sin(_));
            Func<double, double> cos = (_ => Math.Cos(_));
            Func<double, double> sqrt = (_ => Math.Sqrt(_));
            Func<double, double, double> atan2 = ((x, y) => Math.Atan2(x, y));

            const double R = 6371000; // m
            lat1 = lat1 * M_PI / 180;
            lon1 = lon1 * M_PI / 180;
            lat2 = lat2 * M_PI / 180;
            lon2 = lon2 * M_PI / 180;

            double dLat = ((double)lat2 - lat1);
            double dLon = ((double)lon2 - lon1);

            var d = Math.Sqrt(Math.Pow(R * dLat, 2) + Math.Pow(R * dLon, 2));
            return d;
        }
        public double LatToY(double val)
        {
            return (val - center_lat) * scaley;
        }

        public double LonToX(double val)
        {
            return (val - center_lon) * scalex;
        }
        public void ConvertFields(double x, double y, double z, out double X, out double Y, out double Z)
        {
            X = LonToX(x);
            Y = LatToY(y);
            Z = z - center_z;
        }
        //public void ConvertFields1(double B, double L, double H, out double X, out double Y, out double Z)
        //{
        //    B = Math.PI * B / 180 ;
        //    L = Math.PI * L / 180;

        //    Func<double, double> sin = (_ => Math.Sin(_));
        //    Func<double, double> cos = (_ => Math.Cos(_));

        //    // большая полуось
        //    const double a = 6378137;
        //    //const double a2 = 6378245;
        //    // сжатие
        //    const double aa = 1 / 298.257223563;
        //    //const double aa2 = 1 / 298.3;

        //    var e2 = 2 * aa - aa * aa;
        //    //var e2 = 0.00669437999014;

        //    var N = a / Math.Sqrt(1 - e2 * sin(B) * sin(B));

        //    X = (N + H) * cos(B) * cos(L);
        //    Y = (N + H) * cos(B) * sin(L);
        //    Z = ((1 - e2) * N + H) * sin(B);

        //    //X -= center_lon;
        //    //Y -= center_lat;
        //    //Z -= center_z;
        //}
        //public void ConvertFields2(double L, double B, double H, out double Y, out double X, out double Z)
        //{
        //    ConvertFields1(B, L, H, out X, out Y, out Z);
        //    X -= center_lat;
        //    Y -= center_lon;
        //    Z -= center_z;
        //    Y = -Y;
        //}
    }
}
