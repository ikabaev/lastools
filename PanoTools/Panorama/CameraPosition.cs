﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PanoTools.Panorama
{
    public class CameraPosition
    {
        readonly DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
        bool _isIPS3 = false;
        public CameraPosition(string source, bool isIPS3 = false)
        {
            this._isIPS3 = isIPS3;
            this._source = source;
            var arr = source.Split(new[] { '\t',';' }, StringSplitOptions.RemoveEmptyEntries);
            //this.FileName = arr[0].Split('\\').Last(_ => !string.IsNullOrWhiteSpace(_)).Replace("\"", "");
            this.FileName = arr[0].Replace("\"", "");
            this.FileType = arr[1];
            //this.CameraType = int.Parse(arr[2], CultureInfo.InvariantCulture);
            //this.CameraSubtype = int.Parse(arr[3], CultureInfo.InvariantCulture);
            this.SequenceId = int.Parse(arr[isIPS3? 2 : 4], CultureInfo.InvariantCulture);
            //this.TimestampTicks = long.Parse(arr[5], CultureInfo.InvariantCulture);
            this.GpsTimeS = double.Parse(arr[isIPS3 ? 4 : 6], CultureInfo.InvariantCulture);
            //this.GpsTimeUs = int.Parse(arr[7], CultureInfo.InvariantCulture);
            this.Y = double.Parse(arr[isIPS3 ? 5 : 8], CultureInfo.InvariantCulture);
            this.X = double.Parse(arr[isIPS3 ? 6 : 9], CultureInfo.InvariantCulture);
            this.Z = double.Parse(arr[isIPS3 ? 7 : 10], CultureInfo.InvariantCulture);

            this.AttitudeX = double.Parse(arr[isIPS3 ? 8 : 11], CultureInfo.InvariantCulture);
            this.AttitudeY = double.Parse(arr[isIPS3 ? 9 : 12], CultureInfo.InvariantCulture);
            this.AttitudeZ = double.Parse(arr[isIPS3 ? 10 : 13], CultureInfo.InvariantCulture);

            var m = Regex.Match(this.FileName, "\\d+");
            this.PanoID = m.Value;
        }
        readonly string _source;
        public string FileName { get; }
        public string FileType { get; }
        public int CameraType { get; }
        public int CameraSubtype { get; }
        public int SequenceId { get; }
        public long TimestampTicks { get; }
        public double GpsTimeS { get; }
        public DateTime Time => datum.AddSeconds(this.GpsTimeS - 16);
        public string ToDisplay => string.Format("{0:yyyy/MM/dd HH:mm:ss} № {1}", this.Time.AddHours(3), this.PanoID);
        public override string ToString() => string.Format("{0:yyyy/MM/dd HH:mm:ss} № {1}", this.Time.AddHours(3), this.PanoID);

        public string PanoID { get; }
        public int GpsTimeUs { get; }
        public double Y/*Lat*/; //{ get; }
        public double X/*Lon*/; //{ get; }
        public double Z/*Alt*/; //{ get; }

        public double AttitudeX { get; set; }
        public double AttitudeY { get; set; }
        public double AttitudeZ { get; set; }

        public CameraPosition Clone() => new CameraPosition(this._source, _isIPS3);

    }
}
