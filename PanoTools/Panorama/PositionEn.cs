﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PanoTools.Panorama
{
    public class PositionEn : IEnumerator<CameraPosition>
    {
        IEnumerator<string> _linesEn = null;
        bool _isIPS3 = false;
        string _cameraPosPath, _rootPath;

        public PositionEn(string cameraPosPath, bool isIPS3)
        {
            _cameraPosPath = cameraPosPath;
            _rootPath = Path.GetDirectoryName(_cameraPosPath);
            this._isIPS3 = isIPS3;
            _linesEn = File.ReadLines(cameraPosPath).GetEnumerator();
            // пропустим заголовок
            _linesEn.MoveNext();
        }

        CameraPosition _current = null;
        public CameraPosition Current => _current == null ? new CameraPosition(_linesEn.Current.Replace(_rootPath + "\\", ""), _isIPS3) : _current;

        object IEnumerator.Current => this.Current;

        public void Dispose()
        {
            _linesEn.Dispose();
        }

        public bool MoveNext()
        {
            _current = null;
            return _linesEn.MoveNext();
        }

        public void Reset()
        {
            _linesEn.Reset();
        }
    }
}
