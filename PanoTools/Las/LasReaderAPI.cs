﻿using System;
using System.Runtime.InteropServices;
using ICoordsFilter = System.IntPtr;
using LASReader = System.IntPtr;

namespace PanoTools.Las
{
    public class CAPI
    {
        public const string LasReaderDLL = @"LasReaderDLL.dll";
        [DllImport(LasReaderDLL, CharSet = CharSet.Unicode, ThrowOnUnmappableChar = true)]
        public static extern LASReader createLasReader(string file, int cf);
        [DllImport(LasReaderDLL)]
        public static extern void destroyLasReader(LASReader hId);
        [DllImport(LasReaderDLL)]
        public static extern Int64 getNumberOfPoints(LASReader laSReader);
        [DllImport(LasReaderDLL)]
        public static extern bool getNextPoint(LASReader laSReader);
        [DllImport(LasReaderDLL)]
        public static extern void getFields(LASReader laSReader, out double x, out double y, out double z, out double gpsTime, out byte classification);
        [DllImport(LasReaderDLL)]
        public static extern void getFields2(LASReader laSReader, out double x, out double y, out double z, out double gpsTime, out byte classification, out ulong pointSourceID, out byte intensity);
        [DllImport(LasReaderDLL)]
        public static extern void getColor(LASReader laSReader, out ushort r, out ushort g, out ushort b, out ushort a);
        [DllImport(LasReaderDLL)]
        public static extern void convertFields(LASReader laSReader, double x, double y, double z, out double X, out double Y, out double Z);
        [DllImport(LasReaderDLL)]
        public static extern void seek(LASReader laSReader, int position);

        [DllImport(LasReaderDLL, CharSet = CharSet.Unicode, ThrowOnUnmappableChar = true)]
        public static extern ICoordsFilter createTransform(int cf);
        [DllImport(LasReaderDLL)]
        public static extern void TransformCoordinates(ICoordsFilter filter, double x, double y, double z, out double X, out double Y, out double Z);
    }
}
