﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LASReader = System.IntPtr;

namespace PanoTools.Las
{
    public class LasPoint
    {
        public double X;
        public double Y;
        public double Z;
        public double GpsTime;
        public byte Classification;
        public ulong PointSourceID;
        public byte Intensity;
        public ushort R;
        public ushort G;
        public ushort B;
        public ushort A;
        public Color Color => Color.FromArgb(R >> 8, G >> 8, B >> 8);
    }
    public class LasEn : IEnumerator<LasPoint>
    {
        public LASReader _lr = IntPtr.Zero;
        public LasEn(string path)
        {
            _lr = CAPI.createLasReader(path, -1);
        }
        public LasPoint Current
        {
            get
            {
                var p = new LasPoint();
                CAPI.getFields2(_lr, out p.X, out p.Y, out p.Z, out p.GpsTime, out p.Classification, out p.PointSourceID, out p.Intensity);
                CAPI.getColor(_lr, out p.R, out p.G, out p.B, out p.A);
                return p;
            }
        }

        object IEnumerator.Current => this.Current;

        public bool MoveNext()
        {
            return CAPI.getNextPoint(_lr);
        }

        public void Seek(int position)
        {
            CAPI.seek(_lr, position);
        }
        public long GetNumberOfPoints()
        {
            return CAPI.getNumberOfPoints(_lr);
        }
        public void ConvertFields(double x, double y, double z, out double X, out double Y, out double Z)
        {
            CAPI.convertFields(_lr, x, y, z, out X, out Y, out Z);
        }
        public void Reset()
        {
            CAPI.seek(_lr, 0);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LasEn() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            if (_lr != IntPtr.Zero)
            {
                CAPI.destroyLasReader(_lr);
                _lr = IntPtr.Zero;
            }
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
