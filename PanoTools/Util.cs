﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace HelgiLab
{
    public class _En<T> : IEnumerable<T>
    {
        IEnumerator<T> _en;
        public _En(IEnumerator<T> en) { _en = en; }
        public IEnumerator<T> GetEnumerator() => _en;

        IEnumerator IEnumerable.GetEnumerator() => _en;
    }
    public static class GroupingEx
    {
        public static Grouping<TKey2, TElement> Create<TKey, TKey2, TElement>(this IGrouping<TKey, TElement> source, TKey2 key, IEnumerable<TElement> elements)
        {
            return new Grouping<TKey2, TElement>(key, elements);
        }
    }
    public class Cache<TElement> : IReadOnlyCollection<TElement>
    {
        readonly List<TElement> elements;
        public Cache(IEnumerable<TElement> elements)
        {
            if (elements == null)
                throw new ArgumentNullException("elements");

            this.elements = elements.ToList();
        }
        public IEnumerator<TElement> GetEnumerator()
        {
            return this.elements.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        public int Count
        {
            get { return elements.Count; }
        }
        public void Clear()
        {
            this.elements.Clear();
        }
    }
    public class Grouping<TKey, TElement> : Cache<TElement>, IGrouping<TKey, TElement>
    {
        public Grouping(TKey key, IEnumerable<TElement> elements)
            : base(elements)
        {
            this.Key = key;
        }

        public TKey Key { get; private set; }
        public Grouping<TKey, TElement> Create(IEnumerable<TElement> elements)
        {
            return new Grouping<TKey, TElement>(this.Key, elements);
        }
        public TElement Begin { get { return this.First(); } }
        public TElement End { get { return this.Last(); } }
    }
    public class EqualityComparer<TKey> : IEqualityComparer<TKey>
    {
        Func<TKey, TKey, bool> _equals = null;
        Func<TKey, int> _getHashCode = null;

        public static EqualityComparer<TKey> Create(Func<TKey, int> getHashCode, Func<TKey, TKey, bool> equals = null)
        {
            return new EqualityComparer<TKey>()
            {
                _equals = equals,
                _getHashCode = getHashCode
            };
        }
        bool IEqualityComparer<TKey>.Equals(TKey x, TKey y)
        {
            if (_equals == null)
                throw new NotImplementedException();
            else
                return _equals(x, y);
        }

        int IEqualityComparer<TKey>.GetHashCode(TKey obj)
        {
            if (_getHashCode == null)
                throw new NotImplementedException();
            else
                return _getHashCode(obj);
        }
    }
    public class Window<TElement> : IReadOnlyCollection<TElement>
    {
        Queue<TElement> _source = null;
        private Window(IEnumerable<TElement> elements)
        {
            _source = new Queue<TElement>(elements);
        }
        public Window()
        {
            _source = new Queue<TElement>();
        }
        public TElement End
        {
            get
            {
                return _source.Last();
            }
        }
        public TElement Begin
        {
            get { return _source.Peek(); }
        }
        public void Enqueue(TElement item)
        {
            _source.Enqueue(item);
        }
        public TElement Dequeue()
        {
            //if(object.ReferenceEquals(this.Current, item))
            //{
            //	this.Current = null;
            //}
            return _source.Dequeue();
        }
        public Window<TElement> Copy(bool reverse = false)
        {
            return new Window<TElement>(reverse ? _source.Reverse() : _source);
        }
        public TElement this[int index]
        {
            get { return _source.Skip(index).FirstOrDefault(); }
        }
        public int Count
        {
            get { return _source.Count; }
        }

        public IEnumerator<TElement> GetEnumerator()
        {
            return _source.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _source.GetEnumerator();
        }
    }

    #region Disp
    public class DisposeAction : IDisposable
    {
        Action _dispose = null;
        public DisposeAction(Action dispose)
        {
            _dispose = dispose;
        }
        public void Dispose()
        {
            if (_dispose != null)
            {
                _dispose();
            }
        }
    }
    #endregion
    public class DisposableResult<TResult> : IDisposable
    {
        IDisposable _disp = null;
        public TResult Result { get; private set; }

        public DisposableResult(IDisposable disp, TResult result)
        {
            _disp = disp;
            this.Result = result;
        }
        public void Dispose()
        {
            if (_disp != null)
                _disp.Dispose();
        }
        public static implicit operator TResult(DisposableResult<TResult> x)
        {
            return x.Result;
        }
    }
    public static class Util
    {
        public static IEnumerable<T> ToEnumerable<T>(this IEnumerator<T> source)
        {
            return new _En<T>(source);
        }
        public static T Max<T>(params T[] items)
            where T : IComparable<T>
        {
            return items.Max();
        }
        public static T Min<T>(params T[] items)
            where T : IComparable<T>
        {
            return items.Min();
        }
        /// <summary>
        /// Все элементы одинаковые
        /// </summary>
        public static bool Same<T>(this IEnumerable<T> source)
            where T : IEquatable<T>
        {
            bool _same = true;
            bool isFirst = true;
            T prev = default(T);
            foreach (var el in source)
            {
                if (isFirst)
                {
                    isFirst = false;
                    prev = el;
                }
                else if (el.Equals(prev))
                    prev = el;
                else
                {
                    _same = false;
                    break;
                }
            }

            return _same;
        }
        /// <summary>
        /// Все элементы одинаковые
        /// </summary>
        public static bool Same<T, TValue>(this IEnumerable<T> source, Func<T, TValue> evaluator)
            where TValue : IEquatable<TValue>
        {
            return source
                .Select(_ => evaluator(_))
                .Same();
        }
        //public static Func<Control, int, int, Control> OrderChild(this Control container, int stepX, int stepY)
        //{
        //    return (child, x, y) =>
        //    {
        //        child.Size = new Size { Width = stepX - 10, Height = stepY - 10 };
        //        child.Location = new Point { X = stepX * x, Y = stepY * y };
        //        container.Controls.Add(child);
        //        return child;
        //    };
        //}

        //public static Action<bool> Scroll(this ScrollableControl container)
        //{
        //    Action<bool> f = (val) =>
        //    {
        //        container.AutoScroll = val;
        //    };
        //    return f;
        //}
        public static string Serialize<T>(this T source)
        {
            using (MemoryStream st = new MemoryStream())
            {
                XmlSerializer ser = new XmlSerializer(source.GetType());
                ser.Serialize(st, source);
                st.Position = 0;
                using (StreamReader sr = new StreamReader(st, Encoding.UTF8))
                    return sr.ReadToEnd();
            }
        }
        public static T DeSerialize<T>(this string valie)
        {
            using (MemoryStream st = new MemoryStream(Encoding.UTF8.GetBytes(valie)))
            {
                XmlSerializer ser = new XmlSerializer(typeof(T));
                return (T)ser.Deserialize(st);
            }
        }
        /// <summary>
        /// Оконная фунция 
        /// (first, current) => вернуть окно или удалить first элемент
        /// </summary>
        /// <param name="windowComparer">(first, current) => вернуть окно или удалить first элемент</param>
        /// <returns></returns>
        public static IEnumerable<Window<T>> Windowed<T>(this IEnumerable<T> source, Func<T, T, bool> windowComparer)
        {
            var window = new Window<T>();

            foreach (T el in source)
            {
                window.Enqueue(el);

                while (!windowComparer(window.Begin, window.End))
                {
                    window.Dequeue();
                }

                yield return window.Copy();
            }
        }
        /// <summary>
        /// Оконная фунция 
        /// </summary>
        /// <param name="size">размер окна</param>
        /// <param name="step">шаг для текущено элемента</param>
        /// <param name="withTail">возвращать последнее неполное окно</param>
        /// <returns></returns>
        public static IEnumerable<Window<T>> Windowed<T>(this IEnumerable<T> source, int size, int step = 1, bool withTail = false, bool reverse = false)
        {
            int i = 0;
            var window = new Window<T>();
            foreach (T el in source)
            {
                window.Enqueue(el);
                if (window.Count == size)
                {
                    if (i == 0 || i == step)
                    {
                        i = 0;
                        yield return window.Copy(reverse);
                    }
                    i++;
                    window.Dequeue();
                }
            }
            if (withTail && window.Count > 0)
            {
                yield return window.Copy(reverse);
            }
        }
        //public static IEnumerable<TResult> If<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, bool, TResult> @then)
        //{
        //    return source.Select(_ => @then(_));
        //} 
        #region SplitBy
        /// <summary>
        /// Разбивает множество на подмножества последовательно при смене ключа - keySelector
        /// </summary>
        /// <typeparam name="T">тип элемента последовательности</typeparam>
        /// <typeparam name="TKey">тип ключа</typeparam>
        /// <param name="source">последователность</param>
        /// <param name="keySelector">(первый элемент, текущий элемент) => ключ подмножества</param>
        /// <returns></returns>
        public static IEnumerable<Grouping<TKey, T>> SplitBy11<T, TKey>(this IEnumerable<T> source, Func<T, T, TKey> keySelector)
           where TKey : IEquatable<TKey>
        {
            T firstEl = default(T);
            bool prevKeyHasNotValue = true;
            List<T> window = new List<T>();
            TKey prevKey = default(TKey), key;

            foreach (var el in source)
            {
                if (prevKeyHasNotValue)
                {
                    firstEl = el;
                    prevKey = key = keySelector(firstEl, el);
                    prevKeyHasNotValue = !prevKeyHasNotValue;
                }
                else
                    key = keySelector(firstEl, el);


                if (!key.Equals(prevKey) && window.Count > 0)
                {
                    yield return new Grouping<TKey, T>(prevKey, window);
                    window = new List<T>();
                    prevKey = key;
                }
                window.Add(el);
            }
            if (window.Count > 0)
            {
                yield return new Grouping<TKey, T>(prevKey, window);
            }
        }
        /// <summary>
        /// Разбивает множество на подмножества последовательно при смене ключа - keySelector
        /// Отличается от GroupBy тем, что в группу объединяются только соседи с одним ключом, а не все элементы множества с одним ключом
        /// </summary>
        public static IEnumerable<Grouping<TKey, T>> SplitBy<T, TKey>(this IEnumerable<T> source, Func<T, TKey> keySelector)
            where TKey : IEquatable<TKey>
        {
            bool prevKeyHasNotValue = true;
            TKey prevKey = default(TKey);
            List<T> window = new List<T>();

            foreach (T el in source)
            {
                var key = keySelector(el);
                if (prevKeyHasNotValue)
                {
                    prevKey = key;
                    prevKeyHasNotValue = !prevKeyHasNotValue;
                }
                if (!key.Equals(prevKey) && window.Count > 0)
                {
                    yield return new Grouping<TKey, T>(prevKey, window);
                    window = new List<T>();
                    prevKey = key;
                }

                window.Add(el);
            }
            if (window.Count > 0)
            {
                yield return new Grouping<TKey, T>(prevKey, window);
            }
        }
        /// <summary>
        /// Разбивает множество на подмножества последовательно если keySelector == true
        /// Отличается от GroupBy тем, что в группу объединяются только соседи с одним ключом, а не все элементы с одним ключом
        /// </summary>
        public static IEnumerable<Grouping<long, T>> SplitBy2<T>(this IEnumerable<T> source, Func<ICollection<T>, bool> keySelector)
        {
            List<T> window = new List<T>();
            long i = 0;
            foreach (T el in source)
            {
                if (keySelector(window))
                {
                    yield return new Grouping<long, T>(i++, window);
                    window = new List<T>();
                }

                window.Add(el);
            }
            if (window.Count > 0)
            {
                yield return new Grouping<long, T>(i++, window);
            }
        }
        /// <summary>
        /// Разбивает множество на подмножества последовательно если accumulate == null или splitCondition (по умолчанию accumulate == null).
        /// В начале создания очередного подмножества используется накопленное значение accumulate или accInit, если accumulate == null.
        /// Последняя группа возвращается не по условию, а как остаток.
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <typeparam name="TAccumulate">Тип аккомулятора</typeparam>
        /// <param name="source">данные</param>
        /// <param name="accInit">начальный аккомулятор</param>
        /// <param name="accumulate">аккомулятор</param>
        /// <param name="splitCondition">условие разбивки, если accumulate != null</param>
        /// <returns>Последовательность подмножеств данных</returns>
        public static IEnumerable<Grouping<TKey, T>> SplitBy<T, TKey, TAccumulate>(
                this IEnumerable<T> source,
                Func<T, TAccumulate> accInit,
                Func<T, TAccumulate, TAccumulate> accumulate,
                Func<TAccumulate, TKey> splitCondition
            )
            where TAccumulate : class
        {
            var window = new List<T>();
            TAccumulate acc = accInit(default(T));
            foreach (T el in source)
            {
                acc = accumulate(el, acc);
                var sc = splitCondition(acc);
                if (!sc.Equals(default(TKey)))
                {
                    yield return new Grouping<TKey, T>(sc, window);
                    acc = acc ?? accInit(default(T));
                    window = new List<T>();
                }

                window.Add(el);
            }
            if (window.Count > 0)
            {
                yield return new Grouping<TKey, T>(default(TKey), window);
            }
        }
        public static IEnumerable<Grouping<long, T>> SplitBy1111<T, TAccumulate>(this IEnumerable<T> source, TAccumulate accInit, Func<T, TAccumulate, TAccumulate> accumulate)
            where TAccumulate : class
        {
            long key = 0;
            return SplitBy<T, long, TAccumulate>(source, t => accInit, accumulate, acc => acc == default(TAccumulate) ? ++key : default(long));
        }
        #endregion

        public static IEnumerable<T> Seq<T>(this T source, params T[] values)
        {
            return Enumerable.Repeat<T>(source, 1).Concat(values);
        }
        public static IEnumerable<string> FilesByHour(string path, DateTime from, DateTime to)
        {
            return FilesByHour(path, from, () => to);
        }
        public static IEnumerable<string> FilesByHour(string path, DateTime? from, Func<DateTime> to)
        {
            string dir;
            List<string> lst = new List<string>();
            if (!from.HasValue)
            {
                while (null !=
                    (dir = new DirectoryInfo(Path.Combine(path.Seq(lst.ToArray()).ToArray()))
                    .EnumerateDirectories()
                    .Select(x => x.Name)
                    .OrderBy(x => x)
                    .FirstOrDefault()))
                {
                    lst.Add(dir);
                }

                from = new DateTime(int.Parse(lst[0]), int.Parse(lst[1]), int.Parse(lst[2]));
            }
            while (from < DateTime.Now && (from.Value < to() || from.Value.Hour == to().Hour))
            {
                string file = Path.Combine(path, from.Value.Year.ToString(), from.Value.Month.ToString("00"), from.Value.Day.ToString("00"), from.Value.Hour.ToString("00") + ".join");
                if (File.Exists(file))
                    yield return file;

                from = from.Value.AddHours(1);
            }
        }
        public static bool Between<T>(this T x, T x1, T x2)
            where T : IComparable<T>
        {
            return x1.CompareTo(x) <= 0 && x.CompareTo(x2) <= 0;
        }

        public static IEnumerable<T> DoEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T el in source)
            {
                action(el);
                yield return el;
            }
        }
        public static IEnumerable<T> DoEach<T>(this IEnumerable<T> source, Action<T, int> action)
        {
            int i = 0;
            foreach (T el in source)
            {
                action(el, i++);
                yield return el;
            }
        }
        /// <summary>
        /// Описание связи
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TBind"></typeparam>
        /// <param name="source"></param>
        /// <param name="bind"></param>
        /// <returns></returns>
        public static TResult Bind<TSource, TResult>(this TSource source, Func<TSource, TResult> bind)
        {
            return bind(source);
        }
        /// <summary>
        /// Нить наблюдения
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="bind"></param>
        /// <returns></returns>
        public static TResult Observe<TSource, TResult>(this TSource source, Func<TSource, TResult> bind)
        {
            return bind(source);
        }
        /// <summary>
        /// Наибольшая дата кратная ts меньше или равна текущей
        /// </summary>
        /// <param name="value"></param>
        /// <param name="ts"></param>
        /// <param name="markTime">минимальное время</param>
        /// <returns></returns>
        public static DateTime Floor(this DateTime value, TimeSpan ts, DateTime? markTime = null)
        {
            DateTime minValue = markTime ?? DateTime.MinValue;
            var key = (value - minValue).Ticks / ts.Ticks;
            return minValue.AddTicks(key * ts.Ticks);
        }
        /// <summary>
        /// Наименьшая дата кратная ts больше или равна текущей
        /// </summary>
        /// <param name="value"></param>
        /// <param name="ts"></param>
        /// <param name="markTime">минимальное время</param>
        /// <returns></returns>
        public static DateTime Ceiling(this DateTime value, TimeSpan ts, DateTime? markTime = null)
        {
            DateTime minValue = markTime ?? DateTime.MinValue;
            var key = (value - minValue).Ticks / ts.Ticks + 1;
            return minValue.AddTicks(key * ts.Ticks);
        }
        public static long ToKey(this TimeSpan ts, DateTime d, DateTime? markTime = null)
        {
            DateTime dt = markTime ?? DateTime.MinValue;
            return (long)Math.Floor((d - dt).TotalSeconds / ts.TotalSeconds);
        }
        public static DateTime FromKey(this TimeSpan ts, long key, DateTime? markTime = null)
        {
            if (key <= 0)
            {
                throw new ArgumentException("Ключ должен быть положительным");
            }
            DateTime dt = markTime ?? DateTime.MinValue;
            return dt.Add(TimeSpan.FromSeconds(key * ts.TotalSeconds));
        }
        /// <summary>
        /// причина - следствие
        /// объединяет каждый элемент, кроме последнего, с последним 
        /// </summary>
        //public static IEnumerable<Tuple<TSource, TSource>> ReasonEffect1<TSource>(this IEnumerable<TSource> source)
        //{
        //    return (source as List<TSource>)
        //        .IsNull(x => source.ToList())
        //        .If(x => x.Count > 1)
        //        .With(x => new { Last = x[x.Count - 1], Items = x.Take(x.Count - 1) })
        //        .With(x => x.Items
        //            .Select(el => Tuple.Create<TSource, TSource>(el, x.Last))
        //        )
        //        .IsNull(x => x.Empty());
        //}
        public static double Normalize(this double value, double min, double max)
        {
            return (value - min) / (max - min);

        }
        /// <summary>
        /// Действует как Aggregate, но работает по требованию и возвращает последовательность промежуточных и конечных результатов.
        /// </summary>
        public static IEnumerable<TState> Scan<TSource, TState>(this IEnumerable<TSource> source, TState state, Func<TState, TSource, TState> func)
        {
            TState _state = state;
            bool isExistsElement = false;

            foreach (var el in source)
            {
                if (!isExistsElement) isExistsElement = true;
                yield return (_state = func(_state, el));
            }

            if (!isExistsElement) yield return _state;
        }
        /// <summary>
        /// Действует как Aggregate, но работает по требованию и возвращает последовательность промежуточных и конечных результатов.
        /// </summary>
        public static IEnumerable<TSource> Scan<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
        {
            TSource _state = default(TSource);
            //bool isExistsElement = false;

            foreach (var el in source)
            {
                //if (!isExistsElement) isExistsElement = true;
                yield return (_state = func(_state, el));
            }

            //if (!isExistsElement)
            //yield return _state;
        }
        public static IEnumerable<TState> Scan<TSource, TState>(this IEnumerable<TSource> source, Func<TSource, TState> state, Func<TState, TSource, TState> func)
        {
            TState _state = default(TState);
            bool isFirstElement = true;
            foreach (var el in source)
            {
                if (isFirstElement)
                {
                    isFirstElement = false;
                    yield return (_state = state(el));
                }
                else
                {
                    yield return (_state = func(_state, el));
                }
            }
        }
        /// <summary>
        /// Возвращает последовательность, соответствующую кэшированной версии входной последовательности
        /// </summary>
        public static Cache<T> Cache<T>(this IEnumerable<T> source)
        {
            return new Cache<T>(source);
        }
        public static Func<Type1, Type1, IEnumerable<T>> ToFunc<T, Type1>(this IEnumerable<T> source, Func<T, Type1> par, Func<Type1, Type1, IEnumerable<T>> f)
        {
            //return (x, y) => f(x, y);
            return f;
        }
        //public static Func<TParam, TParam, IEnumerable<T>> ToFunc<T, TParam>(this IEnumerable<T> source, Func<T, TParam> par, Func<TParam, TParam, IEnumerable<T>> f)
        //{
        //    return (x, y) => f(x, y);
        //}
        public static IEnumerable<TResult> Normalize<TSource, TResult>(this IReadOnlyCollection<TSource> source, Func<TSource, double> normalizeValue, Func<TSource, double, TResult> result)
        {
            var max = source.Max(el => normalizeValue(el));
            var min = source.Min(el => normalizeValue(el));

            return source
                .Select(el => result(el, (normalizeValue(el) - min) / (max - min)))
                ;
        }
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> source)
        {
            return new ObservableCollection<T>(source);
        }
        /// <summary>
        /// Возвращает последовательность, содержащую только один элемент.
        /// </summary>
        public static IEnumerable<T> Singleton<T>(this T source)
        {
            return Enumerable.Repeat<T>(source, 1);
        }
        public static T GetProperty<T>(this object source, string propName)
        {
            var props = propName.Split('.');
            if (props.Length == 1)
            {
                return (T)source
                .GetType()
                .InvokeMember(propName,
                    BindingFlags.Static |
                    BindingFlags.Public | BindingFlags.NonPublic |
                    BindingFlags.Instance | BindingFlags.GetProperty, null, source, null);
            }
            else
            {
                return (T)props
                    .Aggregate(source, (acc, p) => acc.GetProperty<object>(p));
            }

        }
        public static TSource SetProperty<TSource, TValue>(this TSource source, string propName, TValue value)
        {
            source
                .GetType()
                .InvokeMember(propName,
                    BindingFlags.Static |
                    BindingFlags.Public | BindingFlags.NonPublic |
                    BindingFlags.Instance | BindingFlags.SetProperty, null, source, new Object[] { value });

            return source;
        }
        public static TSource SetProperties<TSource, TOption>(this TSource source, TOption options)
        {
            foreach (var prop in options.GetType().GetProperties()
                .Select(x => new { Key = x.Name, Value = options.GetProperty<object>(x.Name) }))
            {
                source.SetProperty(prop.Key, prop.Value);
            }

            return source;
        }

        public static IEnumerable<T> WithEmpty<T>(this IEnumerable<T> source, Func<T, T> next)
            where T : IComparable<T>
        {
            bool isFirst = true;
            T last = default(T);

            foreach (var el in source)
            {
                if (isFirst)
                {
                    last = el;
                    yield return el;
                    isFirst = false;
                }
                else if (el.CompareTo(next(last)) > 0)
                {
                    while (el.CompareTo(next(last)) > 0)
                    {
                        last = next(last);
                        yield return last;
                    }

                }
                else
                {
                    last = el;
                    yield return el;
                }
            }
        }
        public static T Default<T>(this T value)
        {
            return default(T);
        }
        public static IEnumerable<T> Empty<T>(this IEnumerable<T> source)
        {
            return Enumerable.Empty<T>();
        }
        public static IEnumerable<T> EmptySeq<T>(this T source)
        {
            return Enumerable.Empty<T>();
        }
        public static IEnumerable<Tuple<TOuter, T>> JoinOuter<T, TOuter>(this IEnumerable<T> source, IEnumerable<TOuter> outer, Func<T, TOuter, int> join)
        {
            var en1 = source.GetEnumerator();
            var en2 = outer.GetEnumerator();
            bool next = en1.MoveNext() && en2.MoveNext();

            while (next)
            {
                if (join(en1.Current, en2.Current) == 0)
                {
                    yield return Tuple.Create(en2.Current, en1.Current);
                    next = en1.MoveNext() && en2.MoveNext();
                }
                else if (join(en1.Current, en2.Current) > 0)
                {
                    yield return Tuple.Create(en2.Current, default(T));
                    next = en2.MoveNext();
                }
                else
                {
                    yield return Tuple.Create(default(TOuter), en1.Current);
                    next = en1.MoveNext();
                }
            }
        }
        public static IEnumerable<TResult> JoinOuter<T, TOuter, TResult>(this IEnumerable<T> source, IEnumerable<TOuter> outer, Func<T, TOuter, int> join, Func<T, TOuter, TResult> result)
        {
            var en1 = source.GetEnumerator();
            var en2 = outer.GetEnumerator();
            bool next = en1.MoveNext() && en2.MoveNext();

            while (next)
            {
                if (join(en1.Current, en2.Current) == 0)
                {
                    yield return result(en1.Current, en2.Current);
                    next = en1.MoveNext() && en2.MoveNext();
                }
                else if (join(en1.Current, en2.Current) > 0)
                {
                    yield return result(en1.Current, en2.Current);
                    next = en2.MoveNext();
                }
                else
                {
                    yield return result(en1.Current, en2.Current);
                    next = en1.MoveNext();
                }
            }
        }
        public static IEnumerable<TResult> JoinOuter2<TSource1, TSource2, TCompare, TResult>(this
            IEnumerable<TSource1> source1, IEnumerable<TSource2> source2,
            Func<TSource1, TCompare> joinField1, Func<TSource2, TCompare> joinField2,
            Func<TSource1, TSource2, TResult> result)
            where TCompare : IComparable<TCompare>
        {
            TSource1 last1 = default(TSource1);
            TSource2 last2 = default(TSource2);

            var en1 = source1.GetEnumerator();
            var en2 = source2.GetEnumerator();

            bool next = en1.MoveNext() && en2.MoveNext();

            while (next)
            {
                var compare12 = joinField1(en1.Current).CompareTo(joinField2(en2.Current));
                if (compare12 == 0)
                {
                    yield return result((last1 = en1.Current), (last2 = en2.Current));
                    next = en1.MoveNext() && en2.MoveNext();
                }
                else if (compare12 > 0)
                {
                    yield return result(last1, (last2 = en2.Current));
                    next = en2.MoveNext();
                }
                else
                {
                    yield return result((last1 = en1.Current), last2);
                    next = en1.MoveNext();
                }
            }
        }
        public static double ToOAValue(this TimeSpan value)
        {
            return value.TotalDays;
        }
        public static TimeSpan FromOAValue(this double value)
        {
            return TimeSpan.FromDays(value);
        }
        public static IDisposable ToDispose(this Action action)
        {
            return new DisposeAction(action);
        }
        public static DateTime GetFromGps(double weeknumber, double seconds)
        {
            DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
            DateTime dt = datum.AddDays(weeknumber * 7);
            return dt;
        }
        public static DateTime GetFromGps(double gpsTime)
        {
            double week = gpsTime / 604800.0 + 1653.4391534391534391534391534392;
            double secs = week * 604800 - 1000000000;

            return GetFromGps(week, secs)
                ;
        }
    }
}
